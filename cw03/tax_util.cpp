#include <stdexcept>
#include "tax_util.h"

std::map<TaxUtil::TaxThreshold, double> TaxUtil::m_rate_map = {{FIRST, .19}, {SECOND, .3}, {THIRD, .4}};
std::map<TaxUtil::TaxThreshold, int> TaxUtil::m_excess_map = {{FIRST, 0}, {SECOND, 3702400}, {THIRD, 7404800}};
std::map<TaxUtil::TaxThreshold, int> TaxUtil::m_constant_map = {{FIRST, -49332}, {SECOND, 654124}, {THIRD, 1764844}};
const int TaxUtil::MIN_PROFIT = static_cast<int>(std::ceil(
    // + 0.5 bo m_constant_map[FIRST] jest ujemne
    (((m_constant_map[FIRST] + 0.5) / -ROUND) / m_rate_map[FIRST]) * ROUND // 0.5 grosza i wiecej do calego grosza
));

TaxUtil::TaxThreshold TaxUtil::m_get_threshold(int profit) {
    for (auto it = m_excess_map.rbegin(); it != m_excess_map.rend(); it++) {
        if (profit > it->second) return it->first;
    }
    throw std::runtime_error("Cos poszlo nie tak przy walidacji, podatek jest mniejszy niz 0");
}

int TaxUtil::m_get_base(int profit, TaxThreshold threshold) {
    assert(threshold == m_get_threshold(profit));
    return profit - m_excess_map[threshold];
}

int TaxUtil::get_total(int profit) {
    auto threshold = m_get_threshold(profit);
    // https://www.podatki.biz/artykuly/8_98.htm
    // "końcówki poniżej 0,5 grosza pomija się, a końcówki 0,5 grosza i wyższe zaokrągla się do 1 grosza"
    return m_constant_map[threshold] + std::round(
        std::floor(m_get_base(profit, threshold) * m_rate_map[threshold] * 10.0) / 10.0
    );
}
