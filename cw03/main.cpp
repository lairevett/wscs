// src: https://moodle.wwsi.edu.pl/pluginfile.php/39777/mod_resource/content/6/PP04%20Instrukcje%20warunkowe.pdf
// Przykład 2. Wyznaczanie najmniejszej albo największej liczby spośród trzech liczb.
// Przykład 3. Pierwiastki trójmianu kwadratowego
// Przykład 5. Obliczanie średniej arytmetycznej, geometrycznej, harmonicznej i potęgowej dwóch liczb rzeczywistych
#include <iostream>
#include <cmath>
#include <limits>
#include <map>
#include <cassert>
#include "../shared/console/in.h"
#include "../shared/console/out.h"
#include "../shared/console/out_reader.h"
#include "../shared/console/menu.h"
#include "../shared/validation/error.h"
#include "../shared/validation/decimal_places_validator.h"
#include "../shared/validation/number_greater_validator.h"
#include "../shared/unit_test/case.h"
#include "../shared/misc/constants.h"
#include "tax_util.h"

double wyznacz_min(double x, double y, double z) {
    double min = x;
    if (y < min) min = y;
    if (z < min) min = z;
    return min;
}

double get_max(double x, double y, double z) { return std::max(std::max(x, y), z); }

void run_ex_1() {
    double lb1, lb2, lb3, w;
    int co = 0;
    std::cout << "podaj trzy liczby: ";
    std::cin >> lb1 >> lb2 >> lb3;
    std::cout << "podaj co chcesz wyznaczyc (min: 0, max: 1): ";
    std::cin >> co;
    if (!co) std::cout << "najmniejsza wartosc = " << wyznacz_min(lb1, lb2, lb3);
    else std::cout << "najwieksza wartosc = " << get_max(lb1, lb2, lb3);
    std::cout << "\n";
}

void wyznacz_pierwiastki_rown_kw() {
    float a, b, c;
    float delta;
    std::cout << "Pierwiastki trojmianu kwadratowego" << std::endl;
    std::cout << "Wspolczynnik a = "; std::cin >> a;
    if (a == 0) {
        std::cout << "Niepoprawna wartosc a!";
        return;
    }
    std::cout << "Wspolczynnik b = "; std::cin >> b;
    std::cout << "Wspolczynnik c = "; std::cin >> c;
    delta = b * b - 4 * a * c;
    float skladnik1 = -b / (2 * a);
    float skladnik2 = std::sqrt(std::abs(delta)) / (2 * a);

    std::cout << "***** Wyniki obliczen *****" << std::endl;
    if (delta > 0) {
        std::cout << "x1 = " << skladnik1 - skladnik2 << std::endl;
        std::cout << "x2 = " << skladnik1 + skladnik2;
    } else {
        if (delta < 0) {
            // "Poprawić sposób wyświetlania wartości pierwiastków zespolonych w tym i podobnych przypadkach."
            // chodzi o znaki +/- przy:
            // std::cout << " z1 = " << skladnik1 << "-" << skladnik2 << "i" << std::endl;
            // std::cout << " z2 = " << skladnik1 << "+" << -skladnik2 << "i";
            // ?
            std::cout << " z1 = " << skladnik1 << std::showpos << skladnik2 << std::noshowpos << "i" << std::endl;
            std::cout << " z2 = " << skladnik1 << std::showpos << -skladnik2 << std::noshowpos << "i";
        } else {
            // IEEE-754 :), dzieki "+ 0.0" zamiast -0.000000 wyswietla sie 0.000000
            std::cout << "x0 = " << skladnik1 + 0.0;
        }
    }
}

void run_ex_2() {
    wyznacz_pierwiastki_rown_kw();
    std::cout << "\n";
}

double sr_a(double x, double y) {
    return (x + y) / 2;
}

double sr_g(double x, double y) {
    double prod = x * y;
//    assert(Util::Float::is_greater(prod, 0.0));
    return std::sqrt(prod);
}

double sr_h(double x, double y) {
    double sum = x + y;
//    assert(Util::Float::is_greater(sum , 0.0));
    return (2.0 * x * y) / sum;
}

double sr_p(double x, double y) {
    return std::sqrt((std::pow(x, 2) + std::pow(y, 2)) / 2.0);
}

void run_ex_3() {
    double wynik, liczba1, liczba2;
    int flaga = 1;
    char wybor = ' ';
    std::cout << "jaka srednia obliczyc?";
    std::cout << "\n\ta - arytmetyczna\n\tg - geometryczna\n\th - harmoniczna\n\tp - potegowa\nwybor: ";
    std::cin >> wybor;
    std::cout << "podaj dwie liczby ";
    std::cin >> liczba1 >> liczba2;
    switch (wybor) {
        case 'a':
            wynik = sr_a(liczba1, liczba2);
            break;
        case 'g':
            wynik = sr_g(liczba1, liczba2);
            break;
        case 'h':
            wynik = sr_h(liczba1, liczba2);
            break;
        case 'p':
            wynik = sr_p(liczba1, liczba2);
            break;
        default:
            flaga = 0;
    }
    if (!flaga) std::cout << "nieokreslona srednia";
    else if (std::isnan(wynik) || std::isinf(wynik)) std::cout << "bledne dane - nie mozna wykonac obliczen";
    else std::cout << "wynik = " << wynik;
    std::cout << "\n";
}

void run_ex_4() {
    static constexpr int precision = 2;
    Console::Out::set_precision(precision);
    int profit = std::ceil(Console::In<double>("Dochod: ", {
        new Validation::DecimalPlacesValidator(precision),
        // ponizej ujemna wartosc podatku
        new Validation::NumberGreaterValidator<double>((TaxUtil::MIN_PROFIT - 1) / TaxUtil::ROUND)
    }).read(true, true) * TaxUtil::ROUND);
    Console::Out::write("Podatek: ", TaxUtil::get_total(profit) / TaxUtil::ROUND, " zl", "\n");
}

void display_main_menu() {
    Console::Menu::capture(new std::vector<Console::MenuOption>{
        std::make_pair("Wyjscie", Console::Menu::release),
        std::make_pair("Zadanie 1", run_ex_1),
        std::make_pair("Zadanie 2", run_ex_2),
        std::make_pair("Zadanie 3", run_ex_3),
        std::make_pair("Zadanie 4", run_ex_4)
    });
}

void run_ex_1_unit_tests() {
    std::string ignored = "podaj trzy liczby: podaj co chcesz wyznaczyc (min: 0, max: 1): ";
    // min
    UnitTest::Case::assert_stdout_equals(run_ex_1, "1 2 3\n0\n", ignored + "najmniejsza wartosc = 1\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "1 3 2\n0\n", ignored + "najmniejsza wartosc = 1.000000\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "2 3 1\n0\n", ignored + "najmniejsza wartosc = 1.000000\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "2 1 3\n0\n", ignored + "najmniejsza wartosc = 1.000000\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "3 1 2\n0\n", ignored + "najmniejsza wartosc = 1.000000\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "3 2 1\n0\n", ignored + "najmniejsza wartosc = 1.000000\n");
    // max
    UnitTest::Case::assert_stdout_equals(run_ex_1, "1 2 3\n1\n", ignored + "najwieksza wartosc = 3.000000\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "1 3 2\n1\n", ignored + "najwieksza wartosc = 3.000000\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "2 3 1\n1\n", ignored + "najwieksza wartosc = 3.000000\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "2 1 3\n1\n", ignored + "najwieksza wartosc = 3.000000\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "3 1 2\n1\n", ignored + "najwieksza wartosc = 3.000000\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "3 2 1\n1\n", ignored + "najwieksza wartosc = 3.000000\n");
}

void run_ex_2_unit_tests() {
    std::string ignored = (
        "Pierwiastki trojmianu kwadratowego\n"
        "Wspolczynnik a = Wspolczynnik b = Wspolczynnik c = ***** Wyniki obliczen *****\n"
    );
    UnitTest::Case::assert_stdout_equals(run_ex_2, "0\n1\n1\n", (
        "Pierwiastki trojmianu kwadratowego\nWspolczynnik a = Niepoprawna wartosc a!\n"
    ));
    UnitTest::Case::assert_stdout_equals(run_ex_2, "1\n1\n-2\n", ignored + "x1 = -2.000000\nx2 = 1.000000\n");
    UnitTest::Case::assert_stdout_equals(run_ex_2, "-4\n1\n-2\n", ignored + " z1 = 0.125000-0.695971i\n"
                                                                            " z2 = 0.125000+0.695971i\n");
    UnitTest::Case::assert_stdout_equals(run_ex_2, "1\n0\n0\n", ignored + "x0 = 0.000000\n");
}

void run_ex_3_unit_tests() {
    std::string ignored = (
        "jaka srednia obliczyc?\n"
        "\ta - arytmetyczna\n"
        "\tg - geometryczna\n"
        "\th - harmoniczna\n"
        "\tp - potegowa"
        "\nwybor: podaj dwie liczby "
    );
    UnitTest::Case::assert_stdout_equals(run_ex_3, "q\n0\n0\n", ignored + "nieokreslona srednia\n");
    UnitTest::Case::assert_stdout_equals(
        run_ex_3,
        "g\n1\n-2\n",
        ignored + "bledne dane - nie mozna wykonac obliczen\n"
    );
    UnitTest::Case::assert_stdout_equals(
        run_ex_3,
        "h\n0\n0\n",
        ignored + "bledne dane - nie mozna wykonac obliczen\n"
    );
    UnitTest::Case::assert_stdout_equals(run_ex_3, "a\n1\n2\n", ignored + "wynik = 1.500000\n");
    UnitTest::Case::assert_stdout_equals(run_ex_3, "g\n1\n2\n", ignored + "wynik = 1.414214\n");
    UnitTest::Case::assert_stdout_equals(run_ex_3, "h\n1\n2\n", ignored + "wynik = 1.333333\n");
    UnitTest::Case::assert_stdout_equals(run_ex_3, "p\n1\n2\n", ignored + "wynik = 1.581139\n");
}

void run_ex_4_unit_tests() {
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_4,
        "2596.399999999\n",
        "Maksymalna ilosc cyfr po kropce dziesietnej to 2"
    );
    UnitTest::Case::assert_throws<Validation::Error>(run_ex_4, "2596.39\n", "Wartosc musi byc wieksza niz 2596.39");  // -0.6gr
    // -49332 + round(math.floor(259640 * 0.19 * 10.0) / 10.0)
    UnitTest::Case::assert_stdout_equals(run_ex_4, "2596.40\n", "Podatek: 0.00 zl\n");  // -0.4gr, mozna zaokgraglic do 0gr
    UnitTest::Case::assert_stdout_equals(run_ex_4, "2596.44\n", "Podatek: 0.00 zl\n");  // 0.3gr
    UnitTest::Case::assert_stdout_equals(run_ex_4, "2596.45\n", "Podatek: 0.01 zl\n");  // 0.5gr
    UnitTest::Case::assert_stdout_equals(run_ex_4, "37024\n", "Podatek: 6541.24 zl\n");
    // 654124 + round(math.floor(0.3 * (3702401 - 3702400) * 10.0) / 10.0)
    UnitTest::Case::assert_stdout_equals(run_ex_4, "37024.01\n", "Podatek: 6541.24 zl\n");  // 0.3gr
    UnitTest::Case::assert_stdout_equals(run_ex_4, "37024.02\n", "Podatek: 6541.25 zl\n");  // 0.6gr
    UnitTest::Case::assert_stdout_equals(run_ex_4, "74048\n", "Podatek: 17648.44 zl\n");
    // 1764844 + round(math.floor(0.4 * (7404801 - 7404800) * 10.0) / 10.0)
    UnitTest::Case::assert_stdout_equals(run_ex_4, "74048.01\n", "Podatek: 17648.44 zl\n");  // 0.4gr
    UnitTest::Case::assert_stdout_equals(run_ex_4, "74048.02\n", "Podatek: 17648.45 zl\n");  // 0.8gr
}

int main() {
    Console::Out::set_precision(std::numeric_limits<float>::digits10);
#ifdef NDEBUG
    display_main_menu();
#else
    run_ex_1_unit_tests();
    run_ex_2_unit_tests();
    run_ex_3_unit_tests();
    run_ex_4_unit_tests();
    auto reader = Console::OutReader::get_instance();
    reader->switch_buffers();
    std::cout << UnitTest::Case::get_summary() << std::endl;
    reader->switch_buffers();
#endif
    return EXIT_SUCCESS;
}
