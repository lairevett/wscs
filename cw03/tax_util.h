#ifndef WSCS_CW03_TAX_UTIL_H
#define WSCS_CW03_TAX_UTIL_H

#include <cmath>
#include <cassert>
#include <map>

class TaxUtil {
public:
    static const int MIN_PROFIT;
    static constexpr double ROUND = 100.00;
    static int get_total(int profit);
private:
    enum TaxThreshold { FIRST = 0, SECOND, THIRD };

    static std::map<TaxThreshold, double> m_rate_map;
    static std::map<TaxThreshold, int> m_excess_map;
    static std::map<TaxThreshold, int> m_constant_map;

    static TaxThreshold m_get_threshold(int profit);
    static int m_get_base(int profit, TaxThreshold threshold);
};

#endif // WSCS_CW03_TAX_UTIL_H
