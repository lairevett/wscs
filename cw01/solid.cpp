#include "solid.h"
#include "../shared/console/out.h"

Solid::Solid(double height) :
    m_height(height) {
}

double Solid::get_total_area() {
    return 2 * m_get_base_area() + m_get_sides_area();
}

double Solid::get_volume() {
    return m_get_base_area() * m_height;
}

void Solid::write_ex_2_info() {
    Console::Out::write(
        "Pole calkowite = ", this->get_total_area(), "cm^2",
        ", objetosc = ", this->get_volume(), "cm^3\n"
    );
}
