#ifndef WSCS_CW01_PARABOLIC_TRAJECTORY_H
#define WSCS_CW01_PARABOLIC_TRAJECTORY_H

#include <cmath>
#include "../shared/misc/converter.h"

namespace ParabolicTrajectory {
    double get_end_height(
        double traveled_distance,
        double start_height,
        double start_velocity_kph,
        double angle_deg
    ) {
        double start_velocity_mps = Converter::kph_to_mps(start_velocity_kph);
        double angle_rad = Converter::degrees_to_radians(angle_deg);
        return traveled_distance * std::tan(angle_rad)
               - (GRAVITATIONAL_ACCELERATION * std::pow(traveled_distance, 2)
                  / (2 * std::pow(start_velocity_mps, 2) * std::pow(std::cos(angle_rad), 2)))
               + start_height;
    }
}

#endif // WSCS_CW01_PARABOLIC_TRAJECTORY_H
