#include <cmath>
#include "cylinder.h"

Cylinder::Cylinder(double base_radius, double cylinder_height) :
    m_base_radius(base_radius),
    Solid(cylinder_height) {
}

double Cylinder::m_get_base_area() {
    return M_PI * std::pow(m_base_radius, 2);
}

double Cylinder::m_get_sides_area() {
    return 2 * M_PI * m_base_radius * m_height;
}
