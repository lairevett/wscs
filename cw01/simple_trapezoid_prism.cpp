#include <cmath>
#include "simple_trapezoid_prism.h"

SimpleTrapezoidPrism::SimpleTrapezoidPrism(double bigger_base, double smaller_base, double arm, double height) :
    m_bigger_base(bigger_base),
    m_smaller_base(smaller_base),
    m_arm(arm),
    Solid(height) {
}

double SimpleTrapezoidPrism::m_get_base_height() {
    return std::sqrt(std::pow(m_arm, 2) - std::pow((m_smaller_base - m_bigger_base) / 2, 2));
}

double SimpleTrapezoidPrism::m_get_base_area() {
    return ((m_bigger_base + m_smaller_base) * m_get_base_height()) / 2;
}

double SimpleTrapezoidPrism::m_get_sides_area() {
    return m_bigger_base * m_height + m_smaller_base * m_height + 2 * m_arm * m_height;
}
