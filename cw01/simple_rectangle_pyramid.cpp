#include <cmath>
#include "simple_rectangle_pyramid.h"

SimpleRectanglePyramid::SimpleRectanglePyramid(double base_len_x, double base_len_y, double pyramid_height) :
    m_base_len_x(base_len_x),
    m_base_len_y(base_len_y),
    Solid(pyramid_height) {
}

double SimpleRectanglePyramid::get_total_area() {
    return m_get_base_area() + m_get_sides_area();
}

double SimpleRectanglePyramid::get_volume() {
    return (m_get_base_area() * m_height) / 3.0;
}

double SimpleRectanglePyramid::m_get_base_area() {
    return m_base_len_x * m_base_len_y;
}

double SimpleRectanglePyramid::m_get_side_x_area() {
    return (m_base_len_x * std::sqrt(std::pow((m_base_len_x / 2.0), 2) + std::pow(m_height, 2))) / 2.0;
}

double SimpleRectanglePyramid::m_get_side_y_area() {
    return (m_base_len_y * std::sqrt(std::pow((m_base_len_y / 2.0), 2) + std::pow(m_height, 2))) / 2.0;
}

double SimpleRectanglePyramid::m_get_sides_area() {
    return 2 * m_get_side_x_area() + 2 * m_get_side_y_area();
}
