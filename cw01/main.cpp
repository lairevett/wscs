#include <iostream>
#include <cmath>
#include <string>
#include <limits>
#include <vector>
#include "../shared/console/in.h"
#include "../shared/console/out.h"
#include "../shared/console/out_reader.h"
#include "../shared/console/menu.h"
#include "../shared/validation/error.h"
#include "../shared/validation/decimal_places_validator.h"
#include "../shared/validation/number_between_validator.h"
#include "../shared/validation/number_greater_validator.h"
#include "../shared/unit_test/case.h"
#include "../shared/misc/constants.h"
#include "constants.h"
#include "parabolic_trajectory.h"
#include "simple_trapezoid_prism.h"
#include "simple_rectangle_pyramid.h"
#include "cylinder.h"

void run_ex_1() {
    Console::Out::set_precision(MAX_EX_1_DECIMAL_PLACES);
    double celsius = Console::In<double>("Liczba stopni celsjusza: ", {
        new Validation::DecimalPlacesValidator(MAX_EX_1_DECIMAL_PLACES),
        new Validation::NumberGreaterValidator<double>(MIN_CELSIUS)
    }).read(true, true);
    double fahrenheit = Converter::celsius_to_fahrenheit(celsius);
    Console::Out::write(celsius, "°C = ", fahrenheit, "°F\n");
}

void run_ex_2_simple_prism_isosceles_trapezoid_base() {
    // graniastoslup prosty o podstawie trapezu rownoramiennego
    auto greater_than_0_validator = new Validation::NumberGreaterValidator<double>(0);
    double bigger_base = Console::In<double>(
        "Dlugosc gornej podstawy [cm]: ",
        {greater_than_0_validator}
    ).read(true);
    double smaller_base = Console::In<double>(
        "Dlugosc dolnej podstawy [cm]: ",
        {greater_than_0_validator}
    ).read(true);
    if (bigger_base > smaller_base) std::swap(bigger_base, smaller_base);
    double arm = Console::In<double>(
        "Dlugosc ramienia trapezu [cm]: ",
        {greater_than_0_validator}
    ).read(true);
    double prism_height = Console::In<double>(
        "Wysokosc graniastoslupa [cm]: ",
        {greater_than_0_validator}
    ).read(true);
    delete greater_than_0_validator;
    SimpleTrapezoidPrism trapezoid_prism(bigger_base, smaller_base, arm, prism_height);
    trapezoid_prism.write_ex_2_info();
}

void run_ex_2_simple_issocles_pyramid_rectangle_base() {
    auto greater_than_0_validator = new Validation::NumberGreaterValidator<double>(0);
    double base_len_x = Console::In<double>(
        "Dlugosc pierwszego boku podstawy [cm]: ",
        {greater_than_0_validator}
    ).read(true);
    double base_len_y = Console::In<double>(
        "Dlugosc drugiego boku podstawy [cm]: ",
        {greater_than_0_validator}
    ).read(true);
    double pyramid_height = Console::In<double>(
        "Wysokosc ostroslupa [cm]: ",
        {greater_than_0_validator}
    ).read(true);
    delete greater_than_0_validator;
    SimpleRectanglePyramid rectangle_pyramid(base_len_x, base_len_y, pyramid_height);
    rectangle_pyramid.write_ex_2_info();
}

void run_ex_2_cylinder() {
    auto greater_than_0_validator = new Validation::NumberGreaterValidator<double>(0);
    double base_radius = Console::In<double>(
        "Promien podstawy [cm]: ",
        {greater_than_0_validator}
    ).read(true);
    double cylinder_height = Console::In<double>(
        "Wysokosc walca [cm]: ",
        {greater_than_0_validator}
    ).read(true);
    delete greater_than_0_validator;
    Cylinder cylinder(base_radius, cylinder_height);
    cylinder.write_ex_2_info();
}

void run_ex_2() {
    Console::Out::set_precision(MAX_EX_2_DECIMAL_PLACES);
    Console::Menu::capture(new std::vector<Console::MenuOption>{
        std::make_pair("Powrot", Console::Menu::release),
        std::make_pair(
            "Graniastoslup prosty o podstawie trapezu rownoramiennego",
            run_ex_2_simple_prism_isosceles_trapezoid_base
        ),
        std::make_pair(
            "Ostroslup prosty o podstawie prostokata",
            run_ex_2_simple_issocles_pyramid_rectangle_base
        ),
        std::make_pair("Walec", run_ex_2_cylinder),
    });
}

void run_ex_3() {
    Console::Out::set_precision(MAX_EX_3_DECIMAL_PLACES);
    auto greater_than_0_validator = new Validation::NumberGreaterValidator<double>(0, true);
    auto decimal_places_validator = new Validation::DecimalPlacesValidator(MAX_EX_3_DECIMAL_PLACES);
    double traveled_distance = Console::In<double>(
        "Przebyta droga w poziomie [m]: ",
        {decimal_places_validator, greater_than_0_validator}
    ).read(true);
    double start_height = Console::In<double>(
        "Wysokosc, z ktorej zostala rzucona pilka [m]: ",
        {decimal_places_validator, greater_than_0_validator}
    ).read(true);
    double start_velocity = Console::In<double>(
        "Predkosc poczatkowa [km/h]: ",
        {decimal_places_validator, greater_than_0_validator}
    ).read(true);
    double angle = Console::In<double>(
        "Kat [°]: ",
        {decimal_places_validator, greater_than_0_validator}
    ).read(true);
    delete greater_than_0_validator;
    delete decimal_places_validator;
    double end_height = ParabolicTrajectory::get_end_height(
        traveled_distance, start_height, start_velocity, angle);
    if (end_height < 0) throw Validation::Error("Wysokosc po przebytej drodze jest mniejsza niz 0");
    Console::Out::write("y = ", end_height, "m", "\n");
}

void display_main_menu() {
    Console::Menu::capture(new std::vector<Console::MenuOption>{
        std::make_pair("Wyjscie", Console::Menu::release),
        std::make_pair("Zadanie 1", run_ex_1),
        std::make_pair("Zadanie 2", run_ex_2),
        std::make_pair("Zadanie 3", run_ex_3)
    });
}

void run_sanity_checks() {
    UnitTest::Case::assert_stdin_equals([]() { Console::In<unsigned short>::read_raw(); }, "1\n", "1\n");
    UnitTest::Case::assert_stdin_equals([]() { Console::In<double>::read_raw(); }, "123.45\n", "123.45\n");
    UnitTest::Case::assert_not_throws([]() { Validation::NumberBetweenValidator<unsigned short>(0, 1).validate(0); });
    UnitTest::Case::assert_throws<Validation::Error>(
        []() { Validation::NumberBetweenValidator<unsigned short>(0, 4, true).validate(4); },
        "Nieprawidlowa wartosc, dopuszczalne to 0, 1, 2, 3"
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        []() { Validation::NumberBetweenValidator<unsigned short>(0, 4, false).validate(-4); },
        "Nieprawidlowa wartosc, dopuszczalne to miedzy 0, a 4"
    );
    UnitTest::Case::assert_not_throws([]() { Validation::NumberBetweenValidator<unsigned short>(0, 4).validate(3); });
    UnitTest::Case::assert_throws<Validation::Error>(
        []() { Validation::DecimalPlacesValidator(4).validate(1.11111); },
        "Maksymalna ilosc cyfr po kropce dziesietnej to 4"
    );
    UnitTest::Case::assert_not_throws([]() { Validation::DecimalPlacesValidator(5).validate(1.11111); });
}

void run_ex_1_unit_tests() {
    UnitTest::Case::assert_float_equals(Converter::celsius_to_fahrenheit(0), 32.0);
    UnitTest::Case::assert_float_equals(Converter::celsius_to_fahrenheit(MIN_CELSIUS), -459.67);
    UnitTest::Case::assert_float_equals(
        Converter::celsius_to_fahrenheit((std::numeric_limits<double>::max() - 32) / 1.8),
        std::numeric_limits<double>::max()
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_1,
        "-273.16\n",
        "Wartosc musi byc wieksza niz -273.15"
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_1,
        "123.4567\n",
        "Maksymalna ilosc cyfr po kropce dziesietnej to 3"
    );
    UnitTest::Case::assert_stdout_equals(run_ex_1, "0\n", "0.000°C = 32.000°F\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "123.456\n", "123.456°C = 254.221°F\n");
}

void run_ex_2_unit_tests() {
    SimpleTrapezoidPrism test_prism0(1, 1, 1, 1);
    UnitTest::Case::assert_float_equals(test_prism0.get_total_area(), 6.0);
    UnitTest::Case::assert_float_equals(test_prism0.get_volume(), 1.0);
    SimpleTrapezoidPrism test_prism1(10.2, 13.5, 32.3, 7.4);
    UnitTest::Case::assert_float_equals(test_prism1.get_total_area(), 1417.930536);
    UnitTest::Case::assert_float_equals(test_prism1.get_volume(), 2828.688982);
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_2_simple_prism_isosceles_trapezoid_base,
        "0\n",
        "Wartosc musi byc wieksza niz 0"
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_2_simple_prism_isosceles_trapezoid_base,
        "1\n0\n",
        "Wartosc musi byc wieksza niz 0"
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_2_simple_prism_isosceles_trapezoid_base,
        "1\n1\n0\n",
        "Wartosc musi byc wieksza niz 0"
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_2_simple_prism_isosceles_trapezoid_base,
        "1\n1\n1\n0\n",
        "Wartosc musi byc wieksza niz 0"
    );
    UnitTest::Case::assert_stdout_equals(
        run_ex_2_simple_prism_isosceles_trapezoid_base,
        "1\n2\n3\n4\n",
        "Pole calkowite = 44.874cm^2, objetosc = 17.748cm^3\n"
    );
    SimpleRectanglePyramid test_pyramid0(1, 1, 1);
    UnitTest::Case::assert_float_equals(test_pyramid0.get_total_area(), 3.236068);
    UnitTest::Case::assert_float_equals(test_pyramid0.get_volume(), 0.333333);
    SimpleRectanglePyramid test_pyramid1(7.34, 2.493, 17.399);
    UnitTest::Case::assert_float_equals(test_pyramid1.get_total_area(), 192.304260);
    UnitTest::Case::assert_float_equals(test_pyramid1.get_volume(), 106.125896);
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_2_simple_issocles_pyramid_rectangle_base,
        "0\n",
        "Wartosc musi byc wieksza niz 0"
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_2_simple_issocles_pyramid_rectangle_base,
        "1\n0\n",
        "Wartosc musi byc wieksza niz 0"
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_2_simple_issocles_pyramid_rectangle_base,
        "1\n1\n0\n",
        "Wartosc musi byc wieksza niz 0"
    );
    UnitTest::Case::assert_stdout_equals(
        run_ex_2_simple_issocles_pyramid_rectangle_base,
        "1\n2\n3\n",
        "Pole calkowite = 11.366cm^2, objetosc = 2.000cm^3\n"
    );
}

void run_ex_3_unit_tests() {
    // 4 m/s = 14.4 km/h
    UnitTest::Case::assert_float_equals(ParabolicTrajectory::get_end_height(1.5, 1, 14.4, 45), 1.120469);
    UnitTest::Case::assert_float_equals(Converter::kph_to_mps(14.4), 4);
    UnitTest::Case::assert_float_equals(Converter::degrees_to_radians(45), 0.785398);
    UnitTest::Case::assert_throws<Validation::Error>(run_ex_3, "-1\n", "Wartosc musi byc wieksza lub rowna 0");
    UnitTest::Case::assert_throws<Validation::Error>(run_ex_3, "0\n-1\n", "Wartosc musi byc wieksza lub rowna 0");
    UnitTest::Case::assert_throws<Validation::Error>(run_ex_3, "0\n0\n-1\n", "Wartosc musi byc wieksza lub rowna 0");
    UnitTest::Case::assert_throws<Validation::Error>(run_ex_3, "0\n0\n0\n-1\n", "Wartosc musi byc wieksza lub rowna 0");
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_3,
        "1.11\n",
        "Maksymalna ilosc cyfr po kropce dziesietnej to 1"
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_3,
        "0\n1.11\n",
        "Maksymalna ilosc cyfr po kropce dziesietnej to 1"
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_3,
        "0\n1.1\n1.11\n",
        "Maksymalna ilosc cyfr po kropce dziesietnej to 1"
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_3,
        "0\n0\n0\n1.11\n",
        "Maksymalna ilosc cyfr po kropce dziesietnej to 1"
    );
    UnitTest::Case::assert_stdout_equals(run_ex_3, "1.5\n1\n14.4\n45\n", "y = 1.1m\n");
}

int main() {
# ifdef NDEBUG
    // NDEBUG definiowany w shared/misc/constants.h
    display_main_menu();
# else
    run_sanity_checks();
    run_ex_1_unit_tests();
    run_ex_2_unit_tests();
    run_ex_3_unit_tests();
    auto reader = Console::OutReader::get_instance();
    reader->switch_buffers();
    std::cout << UnitTest::Case::get_summary() << std::endl;
    reader->switch_buffers();
# endif
    return EXIT_SUCCESS;
}
