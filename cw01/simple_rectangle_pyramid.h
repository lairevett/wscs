#ifndef WSCS_CW01_SIMPLE_RECTANGLE_PYRAMID_H
#define WSCS_CW01_SIMPLE_RECTANGLE_PYRAMID_H

#include "solid.h"

class SimpleRectanglePyramid : public Solid {
public:
    SimpleRectanglePyramid(double base_len_x, double base_len_y, double pyramid_height);

    double get_total_area() override;
    double get_volume() override;
private:
    double m_base_len_x;
    double m_base_len_y;

    double m_get_base_area() override;
    double m_get_side_x_area();
    double m_get_side_y_area();
    double m_get_sides_area() override;
};

#endif // WSCS_CW01_SIMPLE_RECTANGLE_PYRAMID_H
