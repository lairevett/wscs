#ifndef WSCS_CW01_CYLINDER_H
#define WSCS_CW01_CYLINDER_H

#include "solid.h"

class Cylinder : public Solid {
public:
    Cylinder(double base_radius, double cylinder_height);
private:
    double m_base_radius;

    double m_get_base_area() override;
    double m_get_sides_area() override;
};

#endif // WSCS_CW01_CYLINDER_H
