#ifndef WSCS_CW01_SIMPLE_TRAPEZOID_PRISM_H
#define WSCS_CW01_SIMPLE_TRAPEZOID_PRISM_H

#include "solid.h"

class SimpleTrapezoidPrism : public Solid {
public:
    SimpleTrapezoidPrism(double bigger_base, double smaller_base, double arm, double height);
private:
    double m_bigger_base;
    double m_smaller_base;
    double m_arm;

    double m_get_base_height();
    double m_get_base_area() override;
    double m_get_sides_area() override;
};

#endif // WSCS_CW01_SIMPLE_TRAPEZOID_PRISM_H
