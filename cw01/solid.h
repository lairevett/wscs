#ifndef WSCS_CW01_SOLID_H
#define WSCS_CW01_SOLID_H

#include "../shared/console/out.h"

class Solid {
public:
    virtual double get_total_area();
    virtual double get_volume();
    void write_ex_2_info();
protected:
    double m_height;

    Solid(double height);

    virtual double m_get_base_area() = 0;
    virtual double m_get_sides_area() = 0;
};

#endif // WSCS_CW01_SOLID_H
