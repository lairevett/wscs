#ifndef WSCS_ZI01_CONSTANTS_H
#define WSCS_ZI01_CONSTANTS_H

#include <map>
#include <vector>
#include <string>
#include "still_lifes.constants.h"
#include "oscillators.constants.h"
#include "wicks_methuselahs.constants.h"
#include "spaceships.constants.h"
#include "guns.constants.h"
#include "puffers.constants.h"

static constexpr char APOCALYPSE_KEY = 'c';
static constexpr unsigned int TICK_RATE = 100;
static constexpr char CELL_MAP[2] = {'.', 'O'};

static constexpr std::string_view GAME_INFO = (
    "Conway's Game of Life, aka Life\n"
    "To jeden z najbardziej znanych przykladow automatu komorkowego, wymyslony w roku 1970 przez brytyjskiego "
    "matematyka Johna Conwaya. Gra zostala spopularyzowana przez Martina Gardnera na lamach Scientific American. Od "
    "momentu publikacji zawsze wzbudzala duze zainteresowanie z powodu zaskakujacego sposobu, w jaki struktury "
    "potrafia ewoluowac. To wlasnie jej pojawienie sie wzbudzilo zainteresowanie automatami komorkowymi wsrod "
    "studentow, ktorzy traktowali ja jako rozrywke oraz fizykow, ktorzy zwrocili uwage na mozliwosci automatow w "
    "zakresie symulatorow fizycznych. Dzisiaj matematykow, ekonomistow i naukowcow z innych dziedzin interesuje "
    "sposob, w jaki przy zastosowaniu tylko kilku prostych regul powstaja skomplikowane struktury.\n\n"
);

static constexpr std::string_view GAME_RULES = (
    "Gra toczy sie na nieskonczonej planszy (plaszczyznie) podzielonej na kwadratowe komorki. Kazda komorka ma osmiu "
    "\"sasiadow\", moze znajdowac sie w jednym z dwoch stanow: \"zywa\" (wlaczona), albo \"martwa\" (wylaczona). Stany "
    "komorek zmieniaja sie w pewnych jednostkach czasu. Stan wszystkich komorek w pewnej jednostce czasu jest uzywany "
    "do obliczenia stanu wszystkich komorek w nastepnej jednostce. Po obliczeniu wszystkie komorki zmieniaja swoj stan "
    "dokladnie w tym samym momencie. Stan komorki zalezy tylko od liczby jej zywych sasiadow. W grze w zycie nie ma "
    "graczy w doslownym tego slowa znaczeniu. Udzial czlowieka sprowadza sie jedynie do ustalenia stanu poczatkowego "
    "komorek.\n\n"
    "Reguly:\n"
    "* martwa komorka, ktora ma dokladnie 3 zywych sasiadow, staje sie zywa w nastepnej jednostce czasu (rodzi sie);\n"
    "* zywa komorka z 2 albo 3 zywymi sasiadami pozostaje nadal zywa;\n"
    "* przy innej liczbie sasiadow umiera (z \"samotnosci\" albo \"zatloczenia\").\n\n"
    "Legenda:\n"
    "* \".\" - martwa komorka\n"
    "* \"O\" - zywa komorka\n\n"
);

static constexpr std::string_view STILL_LIFES_INFO = (
    "Struktury niezmienne, inaczej stabilne lub statyczne, pozostaja identyczne bez wzgledu na krok czasowy (dla "
    "kazdej zywej komorki spelniony jest warunek przetrwania i dla zadnej sposrod martwych nie jest spelniony); "
    "najprostsza taka struktura (block) sklada sie z 4 zywych komorek. Pojawiaja sie bardzo czesto jako produkty "
    "koncowe ewolucji struktur. Tworzy sie je stosunkowo prosto, istnieja schematy wedlug ktorych mozna wymyslac nowe "
    "tego typu struktury.\n\n"
);

static constexpr std::string_view OSCILLATORS_INFO = (
    "Oscylatory zmieniaja sie okresowo, co pewien czas powracaja do swojego stanu pierwotnego; najprostsza taka "
    "struktura sklada sie z trzech zywych komorek polozonych w jednym rzedzie. Najprostsze z nich dosc czesto "
    "pojawiaja sie jako produkty koncowe ewolucji struktur. Okresy oscylatorow najczesciej przyjmuja wartosci 2, 3, 4, "
    "6 lub 8, choc w grze w zycie znaleziono i takie, ktorych okres wynosi prawie 150000. Dla pewnych regul istnieje "
    "nawet oscylator o nazwie \"bialy rekin\", ktory ma okres 150000034. Wiekszosc liczb naturalnych moze byc "
    "dlugosciami okresu oscylatora. Wyjatkami sa jednak liczby 19, 23, 38, oraz 41. Nie wiadomo, czy oscylatory dla "
    "takich dlugosci okresow istnieja, ale jest bardzo prawdopodobne, ze tak jest. Warto dodac, ze dla okresow 34 i 51 "
    "jedyne znane oscylatory skladaja sie z niezaleznie dzialajacych struktur o okresie 2 lub 3 i 17. Przykladowo "
    "oscylator o okresie 34 tworzy sie z oscylatorow dlugosci 2 i 17 w takiej odleglosci, by na siebie nie wplywaly. "
    "Dla cyklu dlugosci 14 istnieje natomiast tylko jeden oscylator (Fontanna). Charakterystyczna cecha oscylatorow o "
    "dluzszych okresach jest podobienstwo tych o cyklu jednakowej dlugosci (np. oscylatory dlugosci 32 przypominaja "
    "zegary z obracajaca sie wskazowka). Ich tworzenie jest dosc trudne (wymaga sporej wyobrazni).\n\n"
);

static constexpr std::string_view WICKS_AND_METHUSELAHS_INFO = (
    "Struktury niestale zmieniaja sie, nie powracajac nigdy do swojego stanu pierwotnego. Jest to w praktyce grupa "
    "laczaca struktury nie nalezace do zadnej z pozostalych kategorii. Z tego tez powodu jest ich najwiecej, a ich "
    "uzyskanie nie sprawia wiekszych trudnosci (losowy uklad komorek wprowadzony jako warunki poczatkowe zwykle "
    "okazuje sie byc struktura niestala). Niektore struktury niestale maja jednak wlasciwosc ciekawego lub "
    "bardzo dlugiego przebiegu rozwoju. Jedna z cech takich struktur jest stosunek liczby krokow, po ktorych nastepuje "
    "stabilizacja do liczby komorek w stadium poczatkowym. Stabilizacja nazywamy zamiane na konfiguracje ukladow "
    "stabilnych, oscylatorow i statkow (zwykle gliderow). Wartosc ta oznaczana bedzie tutaj przez L.\n\n"
    "Najwazniejsze sposrod najprostszych struktur niestalych:\n"
    "* R-Pentomino (F-Pentomino) - chaotyczny rozwoj az do 1103 kroku, emisja kilkudziesieciu gliderow; L221;\n"
    "* B-Heptomino - rozwoj przez ponad 100 krokow az do uzyskania zespolu struktur stabilnych; emituje glidera; L15;\n"
    "* Pi-Heptomino - efektowny rozwoj przez ponad 200 krokow az do uzyskania charakterystycznego zespolu blinkerow "
    "(oscylatorow) i struktur stabilnych, tzw. rezultatu B-Pi; L30;\n"
    "* Bi-Pi-Heptomino - struktura zlozona z dwoch Pi-Heptomino odwroconych wzgledem siebie do gory nogami. Efektownie "
    "rozwija sie, po czym zanika po okolo 10 krokach czasowych;\n"
    "* W-Mino - efektowny rozwoj, zakonczony po 50 krokach powstaniem zbioru blinkerow (oscylatorow) i struktur "
    "stabilnych; L6.\n\n"
    "Najdluzej rozwijajace sie w struktury niezmienne nazwane sa matuzalemami (nazwa pochodzi od Matuzalema). Terminem "
    "diehard (ang. die-hard - niereformowalny) natomiast okresla sie uklad, ktory co prawda znika, ale dopiero po "
    "dlugim czasie.\n\n"
);

static constexpr std::string_view SPACESHIPS_INFO = (
    "Tzw. \"statki\" zwykle zmieniaja sie okresowo - choc okresy nie przekraczaja jednak najczesciej kilkunastu krokow "
    "czasowych - ale wraz z kazdym cyklem przesuwaja sie o stala liczbe pol po planszy w okreslonym kierunku.\n"
    "Najbardziej znanym przykladem takiej struktury, bedacym jednoczesnie niejako symbolem gry w zycie, jest glider "
    "(szybowiec).\n\n"
    "Glider\n"
    "Przez dlugi czas po powstaniu gry w zycie nie bylo jasne, czy istnieje jakikolwiek statek, czyli struktura, ktora "
    "moglaby poruszac sie w nieskonczonosc po planszy. Wyznaczono nawet nagrode za jego odkrycie, w wysokosci 50 "
    "dolarow. Udalo sie w koncu (w wyniku ewolucji R-Pentomino) odnalezc uklad, nazywany po polsku \"szybowcem\", "
    "ktory przesuwa sie po planszy i nigdy nie zastyga.\n Uklad ten stal sie symbolem spolecznosci hakerskiej. W "
    "pazdzierniku 2003 roku Eric Raymond zaproponowal szybowiec na emblemat hakerski. Zostal on bez wiekszych glosow "
    "sprzeciwu zaakceptowany przez spolecznosc, chociaz czesc hakerow uwaza, ze spolecznosc nie powinna miec godla "
    "jako takiego.\n\n"
    "Inne statki\n"
    "Poza tym znane sa takze tzw. statki kosmiczne. Roznia sie one od glidera kierunkiem poruszania sie (pion lub "
    "poziom, a nie ukos) oraz mozliwoscia modyfikacji - poprzez dodawanie komorek w odpowiedni sposob bedziemy "
    "uzyskiwac kolejne statki. W zaleznosci od ich wielkosci nazywane sa LWSS, MWSS, HWSS lub OWSS (skrotowce od "
    "Light/Medium/Heavy/Over Weight Space Ship - Lekki/Sredni/Ciezki/,,Nadciezki\" Statek Kosmiczny). Stosuje sie tez "
    "do nich nazwe Dakota z liczba okreslajaca ich rozmiar. Jeszcze wieksze Dakoty nie moga latac samodzielnie, ale "
    "moga w towarzystwie mniejszych.\n"
    "Poza tym istnieje jeszcze kilka innych statkow o stosunkowo niewielkich rozmiarach (m.in. Maszyna Schicka, "
    "Rzutka). Pozostale (kilkaset) takie struktury sa duze (kilkaset zywych komorek) i trudne do tworzenia. "
    "Wymyslajacy je informatycy nadaja im czesto artystyczna forme, np. ryby czy falujacej wody.\n\n"
);

static constexpr std::string_view GUNS_INFO = (
    "Dziala to oscylatory, ktore co jeden okres \"wyrzucaja\" z siebie jeden statek, ktory odlacza sie i egzystuje "
    "samodzielnie. Najwiecej dzial generuje glidery, poza tym czesc jest zdolna do wytwarzania statkow kosmicznych. "
    "Dlugosc okresu tych struktur waha sie od 30 (najprostszy Gosper Glider Gun) az do kilkudziesieciu tysiecy krokow "
    "czasowych. Ze wzgledu na fakt, ze sa to formy bardzo zaawansowane (od stu do nawet kilku tysiecy zywych komorek), "
    "ich tworzenie jest zwykle bardzo czasochlonne i wymaga praktyki.\n\n"
);

static constexpr std::string_view PUFFERS_INFO = (
    "Puffery, inaczej puffer trainy - dymiace pociagi. Struktury oscylujace (o okresie zwykle w okolicach kilkunastu "
    "krokow) oraz poruszajace sie po planszy, a przy tym pozostawiajace za soba cyklicznie inne struktury, ktore "
    "odlaczaja sie i egzystuja samodzielnie. Najprostsze puffery (skladaja sie one juz z dwudziestu kilku zywych "
    "komorek) - zostawiaja za soba statki lub chaotyczny, stabilny pas, tzw. ruiny (debris). Bardziej zlozone - "
    "oscylatory, dziala czy nawet inne puffery. Powszechnie stosowana metoda tworzenia prostych pufferow jest "
    "odpowiednie skladanie MWSS-ow oraz niewielkiej struktury niestalej, jak np. B-Heptomino. Puffery sa najbardziej "
    "efektownymi strukturami gry w zycie. Przykladowo, maja one mozliwosc przeprowadzenia algorytmu wyznaczajacego "
    "liczby pierwsze. Jednoczesnie, ich tworzenie jest tak trudne, ze nawet doswiadczeni informatycy traktuja je jako "
    "wymagajace nie lada poswiecenia. Wplywa na to miedzy innymi fakt, ze niektore z tych struktur maja po 5000 "
    "komorek zywych w stadium poczatkowym. Do tej pory wynaleziono okolo setki pufferow. Puffera, ktory zostawia za "
    "soba statki, nazywamy rake'iem (ang. grabie, hulaka). Najprostszy rake sklada sie z 2 LWSS-ow i B-Heptomina, w "
    "sklad wszystkich innych rowniez wchodza WSS-y. Najbardziej zlozona struktura tego typu, Spider-Rake, sklada sie z "
    "okolo 1000 komorek w pierwotnym stadium. Istnieje kilkadziesiat odkrytych rake'ow.\n\n"
);


typedef std::pair<std::string_view, std::vector<std::pair<std::string, std::string_view>>> ConfigOptionDescription;
typedef std::map<std::string, ConfigOptionDescription> ConfigOptionMap;
static const ConfigOptionMap CONFIGS = {
    {
        "Niezmienne", {
            STILL_LIFES_INFO, {
                {"Big S", STILL_LIFES_BIG_S_CONFIG},
                {"Cloverleaf Interchange", STILL_LIFES_CLOVERLEAF_INTERCHANGE_CONFIG},
                {"Cthulhu", STILL_LIFES_CTHULHU_CONFIG},
                {"Eater", STILL_LIFES_EATER_CONFIG},
                {"Grin Reagent", STILL_LIFES_GRIN_REAGENT_CONFIG},
                {"Hexagonal Key", STILL_LIFES_HEXAGONAL_KEY_CONFIG},
                {"Honeycomb", STILL_LIFES_HONEYCOMB_CONFIG},
                {"Lake", STILL_LIFES_LAKE_CONFIG}
            }
        }
    },
    {
        "Oscylatory", {
            OSCILLATORS_INFO, {
                {"Blinker", OSCILLATORS_BLINKER_CONFIG},
                {"Blocker", OSCILLATORS_BLOCKER_CONFIG},
                {"Figure Eight", OSCILLATORS_FIGURE_EIGHT_CONFIG},
                {"Pentadecathlon", OSCILLATORS_PENTADECATHLON_CONFIG},
                {"Pulsar", OSCILLATORS_PULSAR_CONFIG},
                {"Queen Bee Shuttle", OSCILLATORS_QUEEN_BEE_SHUTTLE_CONFIG},
                {"Tanner's P46", OSCILLATORS_TANNERS_P46_CONFIG},
                {"Twin Bees Shuttle", OSCILLATORS_TWIN_BEES_SHUTTLE_CONFIG}
            }
        }
    },
    {
        "Niestale", {
            WICKS_AND_METHUSELAHS_INFO, {
                {"Ants", WICKS_METHUSELAHS_ANTS_CONFIG},
                {"B-Heptomino", WICKS_METHUSELAHS_B_HEPTOMINO_CONFIG},
                {"Blinker Fuse", WICKS_METHUSELAHS_BLINKER_FUSE_CONFIG},
                {"Die Hard", WICKS_METHUSELAHS_DIE_HARD_CONFIG},
                {"Justyna", WICKS_METHUSELAHS_JUSTYNA_CONFIG},
                {"Lidka", WICKS_METHUSELAHS_LIDKA_CONFIG},
                {"Pi-Heptomino", WICKS_METHUSELAHS_PI_HEPTOMINO_CONFIG},
                {"R-Heptomino", WICKS_METHUSELAHS_R_HEPTOMINO_CONFIG}
            }
        }
    },
    {
        "Statki", {
            SPACESHIPS_INFO, {
                {"Big Glider", SPACESHIPS_BIG_GLIDER_CONFIG},
                {"Brain", SPACESHIPS_BRAIN_CONFIG},
                {"Copperhead", SPACESHIPS_COPPERHEAD_CONFIG},
                {"Cottonmouth", SPACESHIPS_COTTONMOUTH_CONFIG},
                {"Crab", SPACESHIPS_CRAB_CONFIG},
                {"Dart", SPACESHIPS_DART_CONFIG},
                {"Spider", SPACESHIPS_SPIDER_CONFIG},
                {"Wasp", SPACESHIPS_WASP_CONFIG}
            }
        }
    },
    {
        "Dziala", {
            GUNS_INFO, {
                {"AK94", GUNS_AK94_CONFIG},
                {"B-52 Bomber", GUNS_B52_BOMBER_CONFIG},
                {"Bi-Gun", GUNS_BI_GUN_CONFIG},
                {"Gosper Glider Gun", GUNS_GOSPER_GLIDER_GUN_CONFIG},
                {"New Gun 1", GUNS_NEW_GUN_1_CONFIG},
                {"New Gun 2", GUNS_NEW_GUN_2_CONFIG},
                {"Simkin Glider Gun", GUNS_SIMKIN_GLIDER_GUN_CONFIG},
                {"Vacuum", GUNS_VACUUM_CONFIG}
            }
        }
    },
    {
        "Puffery", {
            PUFFERS_INFO, {
                {"Blinker Puffer", PUFFERS_BLINKER_PUFFER_CONFIG},
                {"Block-Laying Switch Engine", PUFFERS_BLOCK_LAYING_SWITCH_ENGINE_CONFIG},
                {"Hivenudger", PUFFERS_HIVENUDGER_CONFIG},
                {"Noah's Ark", PUFFERS_NOAHS_ARK_CONFIG},
                {"Puffer 1", PUFFERS_PUFFER_1_CONFIG},
                {"Puffer 2", PUFFERS_PUFFER_2_CONFIG},
                {"Pufferfish", PUFFERS_PUFFERFISH_CONFIG},
                {"Slow Puffer", PUFFERS_SLOW_PUFFER_CONFIG}
            }
        }
    }
};

#endif // WSCS_ZI01_CONSTANTS_H
