#ifndef WSCS_ZI01_OSCILLATORS_CONSTANTS_H
#define WSCS_ZI01_OSCILLATORS_CONSTANTS_H

#include <string>

static constexpr std::string_view OSCILLATORS_BLINKER_CONFIG = (
    "OOO0"
);

static constexpr std::string_view OSCILLATORS_BLOCKER_CONFIG = (
    "......O.O0"
    ".....O0"
    "OO..O....O0"
    "OO.O..O.OO0"
    "....OO0"
);

static constexpr std::string_view OSCILLATORS_FIGURE_EIGHT_CONFIG = (
    "OO0"
    "OO.O0"
    "....O0"
    ".O0"
    "..O.OO0"
    "....OO0"
);

static constexpr std::string_view OSCILLATORS_PENTADECATHLON_CONFIG = (
    "..O....O0"
    "OO.OOOO.OO0"
    "..O....O0"
);

static constexpr std::string_view OSCILLATORS_PULSAR_CONFIG = (
    "..OOO...OOO0"
    "0"
    "O....O.O....O0"
    "O....O.O....O0"
    "O....O.O....O0"
    "..OOO...OOO0"
    "0"
    "..OOO...OOO0"
    "O....O.O....O0"
    "O....O.O....O0"
    "O....O.O....O0"
    "0"
    "..OOO...OOO0"
);

static constexpr std::string_view OSCILLATORS_QUEEN_BEE_SHUTTLE_CONFIG = (
    ".........O0"
    ".......O.O0"
    "......O.O0"
    "OO...O..O...........OO0"
    "OO....O.O...........OO0"
    ".......O.O0"
    ".........O0"
);

static constexpr std::string_view OSCILLATORS_TANNERS_P46_CONFIG = (
    "..OO.........0"
    "..O..........0"
    "...O.........0"
    "..OO.........0"
    ".............0"
    ".........OO..0"
    ".........O...0"
    "..........O..0"
    ".........OO..0"
    ".OO..........0"
    ".OO......OO..0"
    "O.......O.O..0"
    ".OO......O...0"
    ".OO.......OOO0"
    "............O0"
    ".............0"
    ".............0"
    ".............0"
    ".............0"
    ".............0"
    ".............0"
    ".OO..........0"
    ".OO..OO......0"
    ".....O.O.....0"
    ".......O.....0"
    ".......OO....0"
);

static constexpr std::string_view OSCILLATORS_TWIN_BEES_SHUTTLE_CONFIG = (
    ".................OO0"
    "OO...............O.O.......OO0"
    "OO.................O.......OO0"
    ".................OOO0"
    "0"
    "0"
    "0"
    ".................OOO0"
    "OO.................O0"
    "OO...............O.O0"
    ".................OO0"
);

#endif // WSCS_ZI01_OSCILLATORS_CONSTANTS_H
