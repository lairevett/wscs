#ifndef WSCS_ZI01_SPACESHIPS_CONSTANTS_H
#define WSCS_ZI01_SPACESHIPS_CONSTANTS_H

#include <string>

static constexpr std::string_view SPACESHIPS_BIG_GLIDER_CONFIG = (
    "...OOO0"
    "...O..OOO0"
    "....O.O0"
    "OO.......O0"
    "O.O....O..O0"
    "O........OO0"
    ".OO0"
    ".O..O.....O.OO0"
    ".O.........OO.O0"
    "...O.O......OO..O0"
    "....OO.O....OO...O0"
    "........O.......O0"
    ".......OOOO...O.O0"
    ".......O.OO...OOOO0"
    "........O...OO.O0"
    ".............OO0"
    ".........O.OOO0"
    "..........O..O0"
);

static constexpr std::string_view SPACESHIPS_BRAIN_CONFIG = (
    ".OOO.........OOO0"
    "O.O.OO.....OO.O.O0"
    "O.O.O.......O.O.O0"
    ".O.OO.OO.OO.OO.O0"
    ".....O.O.O.O0"
    "...O.O.O.O.O.O0"
    "..OO.O.O.O.O.OO0"
    "..OOO..O.O..OOO0"
    "..OO..O...O..OO0"
    ".O....OO.OO....O0"
    ".O.............O0"
);

static constexpr std::string_view SPACESHIPS_COPPERHEAD_CONFIG = (
    ".OO..OO.0"
    "...OO...0"
    "...OO...0"
    "O.O..O.O0"
    "O......O0"
    "........0"
    "O......O0"
    ".OO..OO.0"
    "..OOOO..0"
    "........0"
    "...OO...0"
    "...OO...0"
);

static constexpr std::string_view SPACESHIPS_COTTONMOUTH_CONFIG = (
        "..OO..OO..0"
        "....OO....0"
        "....OO....0"
        ".O.O..O.O.0"
        ".O......O.0"
        "..........0"
        ".O......O.0"
        "..OO..OO..0"
        "...OOOO...0"
        "..........0"
        "OOO....OOO0"
        "..........0"
        "OO......OO0"
        "OO......OO0"
        "..........0"
        ".O......O.0"
        ".O.O..O.O.0"
        "..........0"
        "..OO..OO..0"
        ".O......O.0"
        "..........0"
        "....OO....0"
        "...O..O...0"
        "...O..O...0"
        "..O....O..0"
        "..O....O..0"
        "...OOOO...0"
        "..OO..OO..0"
        "..O....O..0"
        "..O....O..0"
        "..........0"
        "..........0"
        "...OOOO...0"
        "....OO....0"
);

static constexpr std::string_view SPACESHIPS_CRAB_CONFIG = (
    "........OO0"
    ".......OO0"
    ".........O0"
    "...........OO0"
    "..........O0"
    "0"
    ".........O..O0"
    ".OO.....OO0"
    "OO.....O0"
    "..O....O.O0"
    "....OO..O0"
    "....OO0"
);

static constexpr std::string_view SPACESHIPS_DART_CONFIG = (
    ".......O0"
    "......O.O0"
    ".....O...O0"
    "......OOO0"
    "0"
    "....OO...OO0"
    "..O...O.O...O0"
    ".OO...O.O...OO0"
    "O.....O.O.....O0"
    ".O.OO.O.O.OO.O0"
);

static constexpr std::string_view SPACESHIPS_SPIDER_CONFIG = (
    ".........O.......O0"
    "...OO.O.O.OO...OO.O.O.OO0"
    "OOO.O.OOO.........OOO.O.OOO0"
    "O...O.O.....O.O.....O.O...O0"
    "....OO......O.O......OO0"
    ".OO.........O.O.........OO0"
    ".OO.OO...............OO.OO0"
    ".....O...............O0"
);

static constexpr std::string_view SPACESHIPS_WASP_CONFIG = (
    "..........O...O0"
    ".......O.O.OO.OOO0"
    "......O..O......OO.OO0"
    ".OO..OO..O...O..O..OO0"
    ".OO.O.OO..O..O....O..O0"
    "O...O....OO0"
    "O.O.O..O..OO0"
    ".........O0"
    ".OOO0"
);

#endif // WSCS_ZI01_SPACESHIPS_CONSTANTS_H
