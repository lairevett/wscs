#ifndef WSCS_ZI01_GAME_OF_LIFE_H
#define WSCS_ZI01_GAME_OF_LIFE_H

#include <string>
#include <map>
#include <tuple>
#include "constants.h"

class Life {
public:
    Life();
    Life(const std::string_view& cells_config);
    ~Life();
    void play();
    void tick();
private:
    char* M_RENDER_MESSAGE;
    bool m_is_first_tick;
    bool m_is_judgment_day;
    std::map<char, std::tuple<int, int> (Life::*)()> m_coord_input_func_hashmap = {
        {'t', &Life::gen_random_cell_position},
        {'a', &Life::gen_random_cell_position},
        {'n', &Life::read_cell_position},
        {'i', &Life::read_cell_position}
    };
    int m_rng_boundary;
    int m_universe_edge;
    int m_universe_capacity;
    int m_universe_bytes;
    int* m_universe;
    int* m_alive_cells_tmp;
    int* m_dead_cells_tmp;

    void init();
    std::tuple<int, int>* init_cell_space(int count);
    void on_init_finished(std::tuple<int, int>* alive_cells, int alive_cells_count);
    std::tuple<int, int> gen_random_cell_position();
    std::tuple<int, int> read_cell_position();
    int parse_alive_cells(std::tuple<int, int> dest[], const std::string_view& config);
    int read_alive_cells(std::tuple<int, int> dest[], int max_count);
    void create_universe(std::tuple<int, int> cells[], int count);
    unsigned int get_cell_neighbors_count(int index);
    void process_input();
    void update();
    void render();
};

#endif // WSCS_ZI01_GAME_OF_LIFE_H
