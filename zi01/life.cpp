#include <cstring>
#include <cassert>
#include <curses.h>
#include "life.h"
#include "constants.h"
#include "../shared/console/in.h"
#include "../shared/console/out.h"
#include "../shared/console/menu.h"
#include "../shared/util/random_number_generator.h"
#include "../shared/util/marray.h"
#include "../shared/validation/number_greater_validator.h"
#include "../shared/validation/choice_validator.h"
#include "../shared/misc/constants.h"

Life::Life() :
    m_is_first_tick(true),
    m_is_judgment_day(false),
    m_universe(nullptr),
    m_universe_edge(0),
    m_universe_capacity(0),
    m_universe_bytes(0) {
    init();
    // niektore komorki moga byc zignorowane ze wzgledu na taka sama pare (x, y), to maksymalna ilosc komorek
    int max_alive_cells_count = Console::In<int>(
        "Liczba komorek na planszy: ",
        {new Validation::NumberGreaterValidator<int>(0)}
    ).read(true, true);
    m_rng_boundary = static_cast<int>(std::pow(max_alive_cells_count, 2));
    auto alive_cells = init_cell_space(max_alive_cells_count);
    int alive_cells_count = read_alive_cells(alive_cells, max_alive_cells_count);
    on_init_finished(alive_cells, alive_cells_count);
}

Life::Life(const std::string_view& cells_config) :
    m_is_first_tick(true),
    m_is_judgment_day(false),
    m_universe(nullptr),
    m_universe_edge(0),
    m_universe_capacity(0),
    m_universe_bytes(0) {
    init();
    auto alive_cells = init_cell_space(static_cast<int>(cells_config.length()));
    int alive_cells_count = parse_alive_cells(alive_cells, cells_config);
    on_init_finished(alive_cells, alive_cells_count);
}

Life::~Life() {
    free(M_RENDER_MESSAGE);
    free(m_alive_cells_tmp);
    free(m_dead_cells_tmp);
    free(m_universe);
#ifdef NDEBUG
    endwin();
    Console::Out::clear();
#endif
}

void Life::init() {
    Console::Out::clear();
    const char* rmsg_p1 = "Podczas symulacji mozna wcisnac \"";
    int rmsg_p1_len = strlen(rmsg_p1);
    const char* rmsg_p2 = "\", aby powrocic do menu.\n";
    int rmsg_p2_len = strlen(rmsg_p2);
    M_RENDER_MESSAGE = static_cast<char*>(malloc(rmsg_p1_len + rmsg_p2_len));
    memcpy(M_RENDER_MESSAGE, rmsg_p1, rmsg_p1_len);
    M_RENDER_MESSAGE[rmsg_p1_len] = APOCALYPSE_KEY;
    memmove(M_RENDER_MESSAGE + rmsg_p1_len + 1, rmsg_p2, rmsg_p2_len);
}

std::tuple<int, int>* Life::init_cell_space(int count) {
    return static_cast<std::tuple<int, int>*>(malloc(sizeof(std::tuple<int, int>) * count));
}

void Life::on_init_finished(std::tuple<int, int>* alive_cells, int alive_cells_count) {
    create_universe(alive_cells, alive_cells_count);  // modyfikuje m_universe, m_universe_edge, m_universe_capacity, m_universe_bytes
    m_alive_cells_tmp = static_cast<int*>(malloc(m_universe_bytes));
    m_dead_cells_tmp = static_cast<int*>(malloc(m_universe_bytes));
    free(alive_cells);
#ifdef NDEBUG
    initscr();
    cbreak();
    noecho();
    scrollok(stdscr, true);
    nodelay(stdscr, true);
#endif
}

std::tuple<int, int> Life::gen_random_cell_position() {
    assert(m_rng_boundary > 0);
    int x = Util::RandomNumberGenerator<int>::next_int(1, m_rng_boundary);
    int y = Util::RandomNumberGenerator<int>::next_int(1, m_rng_boundary);
    return std::make_tuple(x, y);
}

std::tuple<int, int> Life::read_cell_position() {
    auto greater_than_0_validator = new Validation::NumberGreaterValidator<int>(-1);
    int x = Console::In<int>("Koordynat x komorki: ", {greater_than_0_validator}).read(true);
    int y = Console::In<int>("Koordynat y komorki: ", {greater_than_0_validator}).read(true);
    delete greater_than_0_validator;
    return std::make_tuple(x, y);
}

int Life::parse_alive_cells(std::tuple<int, int> dest[], const std::string_view& config) {
    int count = 0;
    int col = 0;
    int row = 0;
    for (char c : config) {
        if (c == '0') {
            col = 0;
            row++;
            continue;
        }
        if (c == CELL_MAP[1]) dest[count++] = std::make_tuple(col, row);
        col++;
    }
    return count;
}

int Life::read_alive_cells(std::tuple<int, int> dest[], int max_count) {
    char next_coord_choice;
    int cur_count = 0;
    for (int i = 0; i < max_count; i++) {
        if (next_coord_choice != 'a' && next_coord_choice != 'i') {
            next_coord_choice = Console::In<char>(
                "Wygenerowac losowe koordynaty komorki? [(t)ak/(n)ie/t(a)k na wszystkie/n(i)e na wszystkie]: ",
                {new Validation::ChoiceValidator<char>({'t', 'n', 'a', 'i'})}
            ).read(true, true);
        }
        auto coordinates = (this->*m_coord_input_func_hashmap[next_coord_choice])();
        int x = std::get<0>(coordinates);
        int y = std::get<1>(coordinates);
        if (Util::Array<std::tuple<int, int>>::contains(dest, cur_count, coordinates)) {
            Console::Out::write(
                "Komorka o koordynatach (", x, ", ", y, ") znajduje sie juz w tym uniwersum i zostanie zignorowana.\n"
            );
            continue;
        }
        dest[cur_count++] = coordinates;
        Console::Out::write("Komorka o koordynatach (", x, ", ", y, ") zostala dodana do uniwersum.\n");
    }
    return cur_count;
}

void Life::create_universe(std::tuple<int, int> alive_cells[], int alive_cells_count) {
    m_universe_edge = Util::Array<int>::get_max(alive_cells, alive_cells_count) + 10;
    m_universe_capacity = static_cast<int>(std::pow(m_universe_edge, 2));
    m_universe_bytes = static_cast<int>(m_universe_capacity * sizeof(int));
    m_universe = static_cast<int*>(malloc(m_universe_bytes));
    memset(m_universe, 0, m_universe_bytes);
    for (int i = 0; i < alive_cells_count; i++) {
        auto cell = alive_cells[i];
        m_universe[std::get<0>(cell) + m_universe_edge * std::get<1>(cell)] = 1;
//        std::cout << "set at index " << std::get<0>(cell) + universe_edge * std::get<1>(cell) << std::endl;
    }
}

unsigned int Life::get_cell_neighbors_count(int index) {
    int left_index = index - 1;
    int right_index = index + 1;
    int upper_index = index - m_universe_edge;
    int upper_left_index = upper_index - 1;
    int upper_right_index = upper_index + 1;
    int lower_index = index + m_universe_edge;
    int lower_left_index = lower_index - 1;
    int lower_right_index = lower_index + 1;

    int index_edge = m_universe_edge;
    int row = index / index_edge;
    int upper_left_row = upper_left_index / index_edge;
    int left_row = left_index / index_edge;
    int upper_right_row = upper_right_index / index_edge;
    int right_row = right_index / index_edge;
    int lower_right_row = lower_right_index / index_edge;
    int lower_left_row = lower_left_index / index_edge;

    return (left_index >= 0 && row == left_row && m_universe[left_index])
            + (upper_left_index >= 0 && upper_left_row == row - 1 && m_universe[upper_left_index])
            + (upper_index >= 0 && m_universe[upper_index])
            + (upper_right_index >= 0 && upper_right_row == row - 1 && m_universe[upper_right_index])
            + (right_index < m_universe_capacity && row == right_row && m_universe[right_index])
            + (lower_right_index < m_universe_capacity && lower_right_row == row + 1 && m_universe[lower_right_index])
            + (lower_index < m_universe_capacity && m_universe[lower_index])
            + (lower_left_index < m_universe_capacity && lower_left_row == row + 1 && m_universe[lower_left_index]);
}

void Life::process_input() {
    m_is_judgment_day = getch() == APOCALYPSE_KEY;
}

void Life::update() {
    if (m_is_first_tick) {
        // stan uniwersum po inicjalizacji powinien byc wyrenderowany
        m_is_first_tick = false;
        return;
    }
    unsigned int dead_count = 0;
    memset(m_dead_cells_tmp, 0, m_universe_bytes);
    unsigned int alive_count = 0;
    memset(m_alive_cells_tmp, 0, m_universe_bytes);
    for (int i = 0; i < m_universe_capacity; i++) {
        auto neighbors_count = get_cell_neighbors_count(i);
        if (neighbors_count == 3) m_alive_cells_tmp[alive_count++] = i;
        else if (neighbors_count == 2) continue;
        m_dead_cells_tmp[dead_count++] = i;
    }
    for (int i = 0; i < dead_count; i++) m_universe[m_dead_cells_tmp[i]] = 0;
    for (int i = 0; i < alive_count; i++) m_universe[m_alive_cells_tmp[i]] = 1;
}

void Life::render() {
#ifdef NDEBUG
    clear();
    printw(M_RENDER_MESSAGE);
#endif
    for (int i = 0; i < m_universe_capacity; i++) {
        if (i % m_universe_edge == 0) printw("\n");
        char cell = CELL_MAP[m_universe[i]];
#ifdef NDEBUG
        addch(cell);
#else
        Console::Out::write(cell);
#endif
    }
}

void Life::tick() {
    process_input();
    update();
    render();
}

void Life::play() {
    while (!m_is_judgment_day) {
        tick();
        napms(TICK_RATE);
    }
}
