#ifndef WSCS_ZI01_STILL_LIFES_CONSTANTS_H
#define WSCS_ZI01_STILL_LIFES_CONSTANTS_H

#include <string>

static constexpr std::string_view STILL_LIFES_BIG_S_CONFIG = (
    "....OO0"
    "...O..O0"
    "...O.OO0"
    "OO.O0"
    "O..O0"
    ".OO0"
);

static constexpr std::string_view STILL_LIFES_CLOVERLEAF_INTERCHANGE_CONFIG = (
    "....O...O....0"
    "...O.O.O.O...0"
    "...O.O.O.O...0"
    ".OO..O.O..OO.0"
    "O....O.O....O0"
    ".OOOO...OOOO.0"
    ".............0"
    ".OOOO...OOOO.0"
    "O....O.O....O0"
    ".OO..O.O..OO.0"
    "...O.O.O.O...0"
    "...O.O.O.O...0"
    "....O...O....0"
);

static constexpr std::string_view STILL_LIFES_CTHULHU_CONFIG = (
    ".O...O...O.0"
    "O.O.O.O.O.O0"
    "O.O.O.O.O.O0"
    ".OO.O.O.OO.0"
    "....O.O....0"
    "....O.O....0"
    "...OO.OO...0"
    "..O....O...0"
    "..OO.OO....0"
    "...O.O.....0"
    "..O..O.....0"
    "..O.O......0"
    ".OO.OO.....0"
);

static constexpr std::string_view STILL_LIFES_EATER_CONFIG = (
    "...OO0"
    "...O0"
    "OO.O0"
    "O..OO0"
    ".OO....O0"
    "...OOOOO0"
    "...O....OO0"
    "....OO..O0"
    "......O.O0"
    "......O.O.O..O0"
    ".......OO.OOOO0"
    ".........O0"
    ".........O.O0"
    "..........OO0"
);

static constexpr std::string_view STILL_LIFES_GRIN_REAGENT_CONFIG = (
    "...........OO0"
    "....OO.OO..O.0"
    ".....O.O.O.O.0"
    "OO.O.O...O.OO0"
    "O.OO.OO..O...0"
    "....O..O.O.OO0"
    "....O.OO.O.OO0"
    ".....O..O....0"
    "......O......0"
    "...OOO.......0"
    "...O.........0"
);

static constexpr std::string_view STILL_LIFES_HEXAGONAL_KEY_CONFIG = (
    "..OO..0"
    "...O..0"
    ".O....0"
    "O.OOOO0"
    "O.O..O0"
    ".O....0"
);

static constexpr std::string_view STILL_LIFES_HONEYCOMB_CONFIG = (
    "..OO0"
    ".O..O0"
    "O.OO.O0"
    ".O..O0"
    "..OO0"
);

static constexpr std::string_view STILL_LIFES_LAKE_CONFIG = (
    "....OO0"
    "...O..O0"
    "...O..O0"
    ".OO....OO0"
    "O........O0"
    "O........O0"
    ".OO....OO0"
    "...O..O0"
    "...O..O0"
    "....OO0"
);

#endif // WSCS_ZI01_STILL_LIFES_CONSTANTS_H
