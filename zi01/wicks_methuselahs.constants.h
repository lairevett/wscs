#ifndef WSCS_ZI01_WICKS_METHUSELAHS_CONSTANTS_H
#define WSCS_ZI01_WICKS_METHUSELAHS_CONSTANTS_H

#include <string>

static constexpr std::string_view WICKS_METHUSELAHS_ANTS_CONFIG = (
    "OO...OO...OO...OO...OO...OO...OO...OO...OO0"
    "..OO...OO...OO...OO...OO...OO...OO...OO...OO0"
    "..OO...OO...OO...OO...OO...OO...OO...OO...OO0"
    "OO...OO...OO...OO...OO...OO...OO...OO...OO0"
);

static constexpr std::string_view WICKS_METHUSELAHS_B_HEPTOMINO_CONFIG = (
    "O.OO0"
    "OOO0"
    ".O0"
);

static constexpr std::string_view WICKS_METHUSELAHS_BLINKER_FUSE_CONFIG = (
    "OO..O.OO0"
    "OOOOO.O.O0"
    "........O.OOO.OOO.OOO.OOO0"
    "OOOOO.O.O0"
    "OO..O.OO0"
);

static constexpr std::string_view WICKS_METHUSELAHS_DIE_HARD_CONFIG = (
    "......O0"
    "OO0"
    ".O...OOO0"
);

static constexpr std::string_view WICKS_METHUSELAHS_JUSTYNA_CONFIG = (
    ".................O0"
    "................O..O0"
    ".................OOO0"
    ".................O..O0"
    "0"
    "OO................O0"
    ".O................O0"
    "..................O0"
    "0"
    "0"
    "0"
    "0"
    "0"
    "0"
    "0"
    "...................OOO0"
    "...........OOO0"
);

static constexpr std::string_view WICKS_METHUSELAHS_LIDKA_CONFIG = (
    ".O0"
    "O.O0"
    ".O0"
    "0"
    "0"
    "0"
    "0"
    "0"
    "0"
    "0"
    "........O0"
    "......O.O0"
    ".....OO.O0"
    "0"
    "....OOO0"
);

static constexpr std::string_view WICKS_METHUSELAHS_PI_HEPTOMINO_CONFIG = (
    "OOO0"
    "O.O0"
    "O.O0"
);

static constexpr std::string_view WICKS_METHUSELAHS_R_HEPTOMINO_CONFIG = (
    ".OO0"
    "OO0"
    ".O0"
);

#endif // WSCS_ZI01_WICKS_METHUSELAHS_CONSTANTS_H
