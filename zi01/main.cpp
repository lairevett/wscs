#include <vector>
#include "../shared/misc/constants.h"
#include "../shared/console/out.h"
#include "../shared/console/menu.h"
#include "../shared/misc/constants.h"
#include "constants.h"
#include "life.h"

std::function<void()> display_string_view(const std::string_view& str) {
    return [&str]() {
        Console::Out::clear();
        Console::Out::write(str);
        Console::Menu::capture(new std::vector<Console::MenuOption>{
            std::make_pair("Powrot", Console::Menu::release),
        });
    };
}

std::tuple<std::vector<Console::MenuOption>*, std::function<void()>*> parse_config_map() {
    auto back_unsafe = std::make_pair("Powrot", Console::Menu::release_unsafe);
    auto categories = new std::vector<Console::MenuOption>{back_unsafe};
    auto all_items = new std::vector<std::vector<Console::MenuOption>*>();
    for (const auto& category : CONFIGS) {
        auto display_category = [back_unsafe, &category, all_items]() {
            auto category_items = new std::vector<Console::MenuOption>{
                back_unsafe,
                std::make_pair("Informacje", display_string_view(category.second.first))
            };
            for (const auto& item : category.second.second) {
                auto cb = [&item]() {
                    Life life(item.second);
                    life.play();
                };
                category_items->emplace_back(item.first, cb);
            }
            all_items->emplace_back(category_items);
            Console::Out::clear();
            Console::Menu::capture(category_items);
        };
        categories->emplace_back(std::make_pair(category.first, display_category));
    }
    static std::function<void()> free_memory = [categories, all_items]() {
        delete categories;
        for (const auto& item: *all_items) {
            delete item;
        }
        delete all_items;
    };
    return std::make_tuple(categories, &free_memory);
}

std::function<void()> play_game_of_life(const std::vector<Console::MenuOption>& parsed_config_map) {
    return [&parsed_config_map]() {
        Console::Out::clear();
        Console::Menu::capture(new std::vector<Console::MenuOption>{
            std::make_pair("Powrot", Console::Menu::release),
            std::make_pair("Wczytaj koordynaty recznie", []() {
                Life life;
                life.play();
            }),
            std::make_pair("Wczytaj koordynaty ze zdefiniowanych konfiguracji", [&parsed_config_map]() {
                Console::Out::clear();
                Console::Menu::capture(const_cast<std::vector<Console::MenuOption>*>(&parsed_config_map));
            })
        });
    };
}

void display_main_menu(const std::vector<Console::MenuOption>& parsed_config_map) {
    Console::Out::clear();
    Console::Menu::capture(new std::vector<Console::MenuOption>{
        std::make_pair("Wyjscie", Console::Menu::release),
        std::make_pair("Start", play_game_of_life(parsed_config_map)),
        std::make_pair("Informacje", display_string_view(GAME_INFO)),
        std::make_pair("Reguly", display_string_view(GAME_RULES))
    });
}

int main() {
#ifdef NDEBUG
    const auto& [parsed_config_map, free_memory] = parse_config_map();
    display_main_menu(*parsed_config_map);
    (*free_memory)();
#else
    // test_parse_alive_cells
    void (*test_tick)() = []() {
        std::string_view tested_cells_config = (
                "....O...O....0"
                "...O.O.O.O...0"
                "...O.O.O.O...0"
                ".OO..O.O..OO.0"
                "O....O.O....O0"
                ".OOOO...OOOO.0"
                ".............0"
                ".OOOO...OOOO.0"
                "O....O.O....O0"
                ".OO..O.O..OO.0"
                "...O.O.O.O...0"
                "...O.O.O.O...0"
                "....O...O....0"
        );
        Life life(tested_cells_config);
        life.tick();
    };
    UnitTest::Case::assert_stdout_equals(test_tick, "",
                                         "\033[2J\033[H"
                                         "....O...O............."
                                         "...O.O.O.O............"
                                         "...O.O.O.O............"
                                         ".OO..O.O..OO.........."
                                         "O....O.O....O........."
                                         ".OOOO...OOOO.........."
                                         "......................"
                                         ".OOOO...OOOO.........."
                                         "O....O.O....O........."
                                         ".OO..O.O..OO.........."
                                         "...O.O.O.O............"
                                         "...O.O.O.O............"
                                         "....O...O............."
                                         "......................"
                                         "......................"
                                         "......................"
                                         "......................"
                                         "......................"
                                         "......................"
                                         "......................"
                                         "......................"
                                         "......................");
    auto reader = Console::OutReader::get_instance();
    reader->switch_buffers();
    std::cout << UnitTest::Case::get_summary() << std::endl;
    reader->switch_buffers();
#endif
}
