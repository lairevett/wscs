/*EF pp2018*/
/*
Przeliczanie temperatury
if...else if...else
switch
src: https://moodle.wwsi.edu.pl/pluginfile.php/39791/mod_folder/content/0/pp_cw02_1b_temperatura_funkcje_i_warunki%20-rozwiniecie.cpp
*/
#include<iostream>
#include<iomanip>

#include "../shared/console/in.h"
#include "../shared/console/out.h"
#include "../shared/console/out_reader.h"
#include "../shared/validation/number_greater_validator.h"
#include "../shared/validation/error.h"
#include "../shared/misc/constants.h"
#include "../shared/misc/converter.h"
#include "../shared/unit_test/case.h"
#include "constants.h"

using namespace std;
void przeliczFnaC() {
    float c, f;
    cout << "\npodaj temperature w stopniach Fahrenheita: ";
    cin >> f;
    if (f >= -459.6667) {
        c = (f - 32) / 1.8;
        cout << fixed << setprecision(2) << f << " F = " << c << " C";
    }
    else
        cout << "blad, podales temperature ponizej zera bezwzglednego";
}
void przeliczCnaF() {
    Console::Out::set_precision(2);
    double celsius = Console::In<double>("Liczba stopni celsjusza: ", {
        new Validation::NumberGreaterValidator<double>(MIN_CELSIUS)
    }).read(true, true);
    double fahrenheit = Converter::celsius_to_fahrenheit(celsius);
    Console::Out::write(celsius, "°C = ", fahrenheit, "°F\n");
}
void kph_to_mph() {
    Console::Out::set_precision(2);
    double kph = Console::In<double>("Liczba kilometrow na godzine: ", {
        new Validation::NumberGreaterValidator<double>(0)
    }).read(true, true);
    double mph = kph * MILE_COEFFICIENT;
    Console::Out::write(kph, "km/h = ", mph, "mi/h\n");
}
void mph_to_kph() {
    Console::Out::set_precision(2);
    double mph = Console::In<double>("Liczba mil na godzine: ", {
        new Validation::NumberGreaterValidator<double>(0)
    }).read(true, true);
    double kph = mph / MILE_COEFFICIENT;
    Console::Out::write(mph, "mi/h = ", kph, "km/h\n");
}
void run_unit_tests() {
    UnitTest::Case::assert_throws<Validation::Error>(kph_to_mph, "-1\n", "Wartosc musi byc wieksza niz 0");
    UnitTest::Case::assert_stdout_equals(kph_to_mph, "1234567.8910\n", "1234567.89km/h = 767124.92mi/h\n");
    UnitTest::Case::assert_throws<Validation::Error>(mph_to_kph, "-1\n", "Wartosc musi byc wieksza niz 0");
    UnitTest::Case::assert_stdout_equals(mph_to_kph, "1234567.8910\n", "1234567.89mi/h = 1986844.43km/h\n");
    auto reader = Console::OutReader::get_instance();
    reader->switch_buffers();
    std::cout << UnitTest::Case::get_summary() << std::endl;
    reader->switch_buffers();
}
int main() {
# ifdef NDEBUG
    // NDEBUG definiowany w shared/misc/constants.h
    // a propos zadania 3: obliczenia dotyczace bryl sa juz wykonywane przez wiecej niz 3 funkcje w poprzednim cwiczeniu
    cout << "przeliczanie temperatury";
    cout << "\nF->C (1) lub C->F (2) lub km->mi (3) lub mi->km (4): ";
    int wybor;
    cin >> wybor;
//    if (wybor == 1)  // == operator porownania
//    	przeliczFnaC();
//    else if (wybor == 2)
//    	przeliczCnaF();
//    else
//    	cout << "\nnie ma takiej opcji";
    try {
        switch (wybor) {
            case 1: przeliczFnaC(); break;
            case 2: przeliczCnaF(); break;
            case 3: kph_to_mph(); break;
            case 4: mph_to_kph(); break;
            default: cout << "\nnie ma takiej opcji"; break;
        }
    } catch (Validation::Error& exc) {
        Console::Out::write(exc.what(), "\n");
    }
# else
    run_unit_tests();
# endif
    return EXIT_SUCCESS;
}
