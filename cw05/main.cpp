// src: https://moodle.wwsi.edu.pl/pluginfile.php/39787/mod_folder/content/0/pp06/pp06_01_rekurencja.cpp
#include <iostream>
#include <cassert>
#include <cmath>
#include <vector>
#include "../shared/console/in.h"
#include "../shared/console/out.h"
#include "../shared/console/out_reader.h"
#include "../shared/console/menu.h"
#include "../shared/validation/error.h"
#include "../shared/validation/number_between_validator.h"
#include "../shared/validation/number_greater_validator.h"
#include "../shared/unit_test/case.h"
#include "../shared/misc/constants.h"
#define CC_START static unsigned int abs_count = 0; Console::Out::write(++abs_count, ": ", __FUNCTION__, "(",
#define CC_COMMA , ", ",
#define CC_END , ")\n")
#define CC_ARG_1(arg) CC_START arg CC_END
#define CC_ARG_2(arg1, arg2) CC_START arg1 CC_COMMA arg2 CC_END
#define CC_ARG_4(arg1, arg2, arg3, arg4) CC_START arg1 CC_COMMA arg2 CC_COMMA arg3 CC_COMMA arg4 CC_END


std::pair<int, unsigned int> nwd(int a, int b, unsigned int& rel_count) {
    assert(a > b && b > 0);
    CC_ARG_2(a, b);
    if (a % b != 0) return nwd(b, a % b, ++rel_count);
    return std::make_pair(b, abs_count);
}

void run_ex_1() {
    std::vector<std::pair<int, int>> checks = {
        {78, 34},       // 4 calle, nwd == 2
        {7236485, 40},  // 2 calle, nwd == 5
        {7250080, 40}   // 1 call, nwd == 40
    };
    for (auto& check : checks) {
        unsigned int rel_count = 1;
        auto result = nwd(check.first, check.second, rel_count);
        Console::Out::write(
            "dla ", check.first, " i ", check.second, ": \n"
            "\t- wszystkie calle: ", result.second, " (w tym ", rel_count, " na obliczenie tego wyniku)", "\n"
            "\t- wynik: ", result.first, "\n"
        );
    }
}

long long silnia(int n)
{
    CC_ARG_1(n);
    if (n == 0)
        return 1;
    return n * silnia(n - 1);
}

long long fibo(int n)
{
    CC_ARG_1(n);
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fibo(n - 1) + fibo(n - 2);
}

void hanoi(int n, char x, char y, char z)
{
    CC_ARG_4(n, x, y, z);
    if (n == 1)
    {
        std::cout << x << "->" << z << "\n";
        return;
    }
    hanoi(n - 1, x, z, y);
    hanoi(1, x, y, z);
    hanoi(n - 1, y, x, z);
}

void run_ex_2_silnia() {
    int n = Console::In<int>("Podaj n: ", {
        new Validation::NumberBetweenValidator<int>(0, 21, false),
    }).read(true, true);
    Console::Out::write(n, "! = ", silnia(n), "\n");
}

void run_ex_2_fibo() {
    int n = Console::In<int>("Podaj n: ", {
        new Validation::NumberGreaterValidator<int>(-1),
    }).read(true, true);
    Console::Out::write(n, " liczba Fibonacciego: ", fibo(n), "\n");
}

void run_ex_2_hanoi() {
    int n = Console::In<int>("Podaj n: ", {
        new Validation::NumberGreaterValidator<int>(2),
    }).read(true, true);
    hanoi(n, 'A', 'B', 'C');
}

void run_ex_2() {
    Console::Menu::capture(new std::vector<Console::MenuOption>{
        std::make_pair("Powrot", Console::Menu::release),
        std::make_pair("Silnia", run_ex_2_silnia),
        std::make_pair("Fibonacci", run_ex_2_fibo),
        std::make_pair("Hanoi", run_ex_2_hanoi)
    });
}

int coztojest(int a, int b){
    if(b == 0)
        return 0;
    if(b % 2 == 0)
        return coztojest(a+a, b/2);
    return coztojest(a+a, b/2) +a;
    // a) Jakie będą wyniki wywołań coztojest(2, 25) i coztojest(3, 11) ?
    // coztojest(2, 25) == 50, coztojest(3, 11) == 33
    // b) Co oblicza funkcja coztojest(a, b), zakładając, że  a i b są liczbami całkowitymi dodatnimi?
    // a * b
    // c) Co będzie obliczać funkcja coztojest(a, b) (zakładając, że  a i b są liczbami całkowitymi dodatnimi) , gdy znak + zostanie zastąpiony przez *, a instrukcja return 0 przez instrukcję return 1?
    // a ^ b
}

void display_main_menu() {
    Console::Menu::capture(new std::vector<Console::MenuOption>{
        std::make_pair("Wyjscie", Console::Menu::release),
        std::make_pair("Zadanie 1", run_ex_1),
        std::make_pair("Zadanie 2", run_ex_2),
    });
}

void run_ex_1_unit_tests() {
    UnitTest::Case::assert_stdout_equals(
        run_ex_1,
        "",
        "1: nwd(78, 34)\n"
        "2: nwd(34, 10)\n"
        "3: nwd(10, 4)\n"
        "4: nwd(4, 2)\n"
        "dla 78 i 34: \n"
        "\t- wszystkie calle: 4 (w tym 4 na obliczenie tego wyniku)\n"
        "\t- wynik: 2\n"
        "5: nwd(7236485, 40)\n"
        "6: nwd(40, 5)\n"
        "dla 7236485 i 40: \n"
        "\t- wszystkie calle: 6 (w tym 2 na obliczenie tego wyniku)\n"
        "\t- wynik: 5\n"
        "7: nwd(7250080, 40)\n"
        "dla 7250080 i 40: \n"
        "\t- wszystkie calle: 7 (w tym 1 na obliczenie tego wyniku)\n"
        "\t- wynik: 40\n"
    );
}

void run_ex_2_unit_tests() {
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_2_silnia,
        "-1\n",
        "Nieprawidlowa wartosc, dopuszczalne to miedzy 0, a 21"
    );
    UnitTest::Case::assert_throws<Validation::Error>(
        run_ex_2_silnia,
        "21\n",
        "Nieprawidlowa wartosc, dopuszczalne to miedzy 0, a 21"
    );
    UnitTest::Case::assert_stdout_equals(
        run_ex_2_silnia,
        "0\n",
        "1: silnia(0)\n"
        "0! = 1\n"
    );
    UnitTest::Case::assert_stdout_equals(
        run_ex_2_silnia,
        "7\n",
        "2: silnia(7)\n"
        "3: silnia(6)\n"
        "4: silnia(5)\n"
        "5: silnia(4)\n"
        "6: silnia(3)\n"
        "7: silnia(2)\n"
        "8: silnia(1)\n"
        "9: silnia(0)\n"
        "7! = 5040\n"
    );
    UnitTest::Case::assert_throws<Validation::Error>(run_ex_2_fibo, "-1\n", "Wartosc musi byc wieksza niz -1");
    UnitTest::Case::assert_stdout_equals(
        run_ex_2_fibo,
        "0\n",
        "1: fibo(0)\n"
        "0 liczba Fibonacciego: 0\n"
    );
    UnitTest::Case::assert_stdout_equals(
        run_ex_2_fibo,
        "5\n",
        "2: fibo(5)\n"
        "3: fibo(4)\n"
        "4: fibo(3)\n"
        "5: fibo(2)\n"
        "6: fibo(1)\n"
        "7: fibo(0)\n"
        "8: fibo(1)\n"
        "9: fibo(2)\n"
        "10: fibo(1)\n"
        "11: fibo(0)\n"
        "12: fibo(3)\n"
        "13: fibo(2)\n"
        "14: fibo(1)\n"
        "15: fibo(0)\n"
        "16: fibo(1)\n"
        "5 liczba Fibonacciego: 5\n"
    );
    UnitTest::Case::assert_throws<Validation::Error>(run_ex_2_hanoi, "2\n", "Wartosc musi byc wieksza niz 2");
    UnitTest::Case::assert_stdout_equals(
        run_ex_2_hanoi,
        "3\n",
        "1: hanoi(3, A, B, C)\n"
        "2: hanoi(2, A, C, B)\n"
        "3: hanoi(1, A, B, C)\n"
        "A->C\n"
        "4: hanoi(1, A, C, B)\n"
        "A->B\n"
        "5: hanoi(1, C, A, B)\n"
        "C->B\n"
        "6: hanoi(1, A, B, C)\n"
        "A->C\n"
        "7: hanoi(2, B, A, C)\n"
        "8: hanoi(1, B, C, A)\n"
        "B->A\n"
        "9: hanoi(1, B, A, C)\n"
        "B->C\n"
        "10: hanoi(1, A, B, C)\n"
        "A->C\n"
    );
    UnitTest::Case::assert_stdout_equals(
        run_ex_2_hanoi,
        "5\n",
        "11: hanoi(5, A, B, C)\n"
        "12: hanoi(4, A, C, B)\n"
        "13: hanoi(3, A, B, C)\n"
        "14: hanoi(2, A, C, B)\n"
        "15: hanoi(1, A, B, C)\n"
        "A->C\n"
        "16: hanoi(1, A, C, B)\n"
        "A->B\n"
        "17: hanoi(1, C, A, B)\n"
        "C->B\n"
        "18: hanoi(1, A, B, C)\n"
        "A->C\n"
        "19: hanoi(2, B, A, C)\n"
        "20: hanoi(1, B, C, A)\n"
        "B->A\n"
        "21: hanoi(1, B, A, C)\n"
        "B->C\n"
        "22: hanoi(1, A, B, C)\n"
        "A->C\n"
        "23: hanoi(1, A, C, B)\n"
        "A->B\n"
        "24: hanoi(3, C, A, B)\n"
        "25: hanoi(2, C, B, A)\n"
        "26: hanoi(1, C, A, B)\n"
        "C->B\n"
        "27: hanoi(1, C, B, A)\n"
        "C->A\n"
        "28: hanoi(1, B, C, A)\n"
        "B->A\n"
        "29: hanoi(1, C, A, B)\n"
        "C->B\n"
        "30: hanoi(2, A, C, B)\n"
        "31: hanoi(1, A, B, C)\n"
        "A->C\n"
        "32: hanoi(1, A, C, B)\n"
        "A->B\n"
        "33: hanoi(1, C, A, B)\n"
        "C->B\n"
        "34: hanoi(1, A, B, C)\n"
        "A->C\n"
        "35: hanoi(4, B, A, C)\n"
        "36: hanoi(3, B, C, A)\n"
        "37: hanoi(2, B, A, C)\n"
        "38: hanoi(1, B, C, A)\n"
        "B->A\n"
        "39: hanoi(1, B, A, C)\n"
        "B->C\n"
        "40: hanoi(1, A, B, C)\n"
        "A->C\n"
        "41: hanoi(1, B, C, A)\n"
        "B->A\n"
        "42: hanoi(2, C, B, A)\n"
        "43: hanoi(1, C, A, B)\n"
        "C->B\n"
        "44: hanoi(1, C, B, A)\n"
        "C->A\n"
        "45: hanoi(1, B, C, A)\n"
        "B->A\n"
        "46: hanoi(1, B, A, C)\n"
        "B->C\n"
        "47: hanoi(3, A, B, C)\n"
        "48: hanoi(2, A, C, B)\n"
        "49: hanoi(1, A, B, C)\n"
        "A->C\n"
        "50: hanoi(1, A, C, B)\n"
        "A->B\n"
        "51: hanoi(1, C, A, B)\n"
        "C->B\n"
        "52: hanoi(1, A, B, C)\n"
        "A->C\n"
        "53: hanoi(2, B, A, C)\n"
        "54: hanoi(1, B, C, A)\n"
        "B->A\n"
        "55: hanoi(1, B, A, C)\n"
        "B->C\n"
        "56: hanoi(1, A, B, C)\n"
        "A->C\n"
    );
}

int main() {
#ifdef NDEBUG
    display_main_menu();
#else
    run_ex_1_unit_tests();
    run_ex_2_unit_tests();
    auto reader = Console::OutReader::get_instance();
    reader->switch_buffers();
    std::cout << UnitTest::Case::get_summary() << std::endl;
    reader->switch_buffers();
#endif
    return EXIT_SUCCESS;
}
