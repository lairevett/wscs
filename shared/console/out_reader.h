#ifndef WSCS_CONSOLE_OUT_READER_H
#define WSCS_CONSOLE_OUT_READER_H

#include <string>
#include <sstream>

namespace Console {
    class OutReader {
    public:
        // przekierowanie stdout na string, do unit testow
        OutReader(OutReader &other) = delete;
        ~OutReader();

        void operator=(const OutReader &) = delete;

        static OutReader* get_instance();
        void clear_stream();
        std::string get_last();
        void switch_buffers();
    private:
        static OutReader* m_s_singleton;
        std::stringstream m_stream;
        std::streambuf* m_prev;
        std::string m_last;

        OutReader();
    };
}

#endif // WSCS_CONSOLE_OUT_READER_H
