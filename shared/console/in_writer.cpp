#include <iostream>
#include <cassert>
#include "in_writer.h"
#include "../util/mstring.h"

namespace Console {
    InWriter* InWriter::m_s_singleton = nullptr;

    InWriter::InWriter() :
        m_prev(std::cin.rdbuf(m_stream.rdbuf())) {
    }

    InWriter::~InWriter() {
        // przekierowanie do poprzedniego strumienia
        switch_stream();
    }

    class InWriter* InWriter::get_instance() {
        if (m_s_singleton == nullptr) m_s_singleton = new InWriter();
        return m_s_singleton;
    }

    void InWriter::clear_stream() {
        Util::String::clear_stream(m_stream);
    }

    void InWriter::switch_stream() {
        std::cin.rdbuf(m_prev);
    }

    void InWriter::set_next(const std::string& next) {
        assert(Util::String::ends_with(next, "\n"));  // sanity check, bez newline'a input nie zostaje wyslany
        clear_stream();
        m_stream << next << std::flush;
    }

    std::string InWriter::get_next() {
        return m_stream.str();
    }
};
