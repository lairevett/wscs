#include <iostream>
#include "out_reader.h"
#include "../util/mstring.h"

namespace Console {
    OutReader* OutReader::m_s_singleton = nullptr;

    OutReader::OutReader() :
        m_prev(std::cout.rdbuf(m_stream.rdbuf())) {
    }

    OutReader::~OutReader() {
        // przekierowanie do poprzedniego strumienia
        this->switch_buffers();
    }

    OutReader* OutReader::get_instance() {
        if (m_s_singleton == nullptr) m_s_singleton = new OutReader();
        return m_s_singleton;
    }

    void OutReader::clear_stream() {
        Util::String::clear_stream(m_stream);
    }

    std::string OutReader::get_last() {
        if (m_stream.rdbuf()->in_avail() > 0) {
            m_last = this->m_stream.str();
            Util::String::clear_stream(m_stream);
        }
        return m_last;
    }

    void OutReader::switch_buffers() {
        m_prev = std::cout.rdbuf(m_prev);
    }
}
