#include <cassert>
#include "../validation/error.h"
#include "../validation/number_between_validator.h"
#include "menu.h"
#include "in.h"
#include "out.h"

namespace Console {
    std::stack<MenuContext> Menu::m_s_ctx_stack = std::stack<MenuContext>();

    void Menu::m_process_input() {
        try {
            auto top = m_s_ctx_stack.top();
            unsigned short option = top.first->read(true);
            (top.second->begin() + option)->second();
        } catch (Validation::Error& exc) {
            Console::Out::write(exc.what(), "\n");
        }
    }

    std::string Menu::m_get_enumerated_options(std::vector<MenuOption> *option_list) {
        int number = 0;
        std::stringstream stream;
        for (auto& option : *option_list) {
            stream << number++ << ". " << option.first << "\n";
        }
        return stream.str();
    }

    void Menu::capture(std::vector<MenuOption>* option_list) {
        bool was_looping = !m_s_ctx_stack.empty();
        m_s_ctx_stack.push(
            std::make_pair(
                new Console::In<unsigned short>(
                    m_get_enumerated_options(option_list) + "Numer: ", {
                        new Validation::NumberBetweenValidator<unsigned short>(0, option_list->size(), true)
                    }
                ),
                option_list
            )
        );
        if (was_looping) {
            m_process_input();
            return;
        }
        while (!m_s_ctx_stack.empty()) {
            m_process_input();
        }
    }

    void Menu::release() {
        Console::Out::clear();
        assert(!m_s_ctx_stack.empty());
        auto top = m_s_ctx_stack.top();
        delete top.first;
        delete top.second;
        m_s_ctx_stack.pop();
        if (m_s_ctx_stack.empty()) return;
        m_process_input();
    }

    void Menu::release_unsafe() {
        // pamiec na liste opcji musi byc uwolniona recznie
        Console::Out::clear();
        assert(!m_s_ctx_stack.empty());
        m_s_ctx_stack.pop();
        if (m_s_ctx_stack.empty()) return;
        m_process_input();
    }
}
