#include <iostream>
#include "in.h"
#include "out.h"
#include "out_reader.h"
#include "../shared/misc/constants.h"

namespace Console {
    template class In<unsigned short>;
    template class In<double>;
    template class In<int>;
    template class In<long long int>;
    template class In<char>;

    template<typename type>
    In<type>::In() = default;

    template<typename type>
    In<type>::In(const std::string& msg) :
        m_msg(msg) {
    }

    template<typename type>
    In<type>::In(const std::string& msg, const std::vector<Validation::BaseValidator<type>*>& validators) :
        m_msg(msg),
        m_validators(validators) {
    }

    template<typename type>
    type In<type>::read_raw() {
        type var;
        std::cin >> var;
        return var;
    }

    template<typename type>
    void In<type>::validate(bool free_validators) {
        for (const auto& validator : m_validators) {
            validator->validate(m_value);
            if (free_validators) delete validator;
        }
    }

    template<typename type>
    type In<type>::read(bool validate, bool free_validators) {
        Console::Out::write(m_msg);
# ifndef NDEBUG
        Console::OutReader::get_instance()->clear_stream();
# endif
        m_value = read_raw();
        if (validate) this->validate(free_validators);
        return m_value;
    }
}
