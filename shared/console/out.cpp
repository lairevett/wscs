#include "out.h"

namespace Console {
    int Out::m_s_precision = 0;

    void Out::write() {}

    int Out::set_precision(int precision) {
        int prev = m_s_precision;
        m_s_precision = precision;
        return prev;
    }

    void Out::clear() {
        // CSI[2J czysci konsole, CSI[H przesuwa kursor do lewego gornego rogu
        // posix-specific, nie zadziala na windowsie
        Out::write(u8"\x1B[2J\x1B[H");
    }
}
