#ifndef WSCS_CONSOLE_MENU_H
#define WSCS_CONSOLE_MENU_H

#include <stack>
#include <string>
#include <functional>
#include "../console/in.h"

namespace Console {
    typedef std::pair<std::string, std::function<void()>> MenuOption;
    typedef std::pair<Console::In<unsigned short>*, std::vector<MenuOption>*> MenuContext;

    class Menu {
    public:
        Menu() = delete;
        Menu(Menu &other) = delete;

        void operator=(const Menu&) = delete;

        static void capture(std::vector<MenuOption>* option_list);
        static void release();
        static void release_unsafe();
    private:
        static std::stack<MenuContext> m_s_ctx_stack;
        static void m_process_input();
        static std::string m_get_enumerated_options(std::vector<MenuOption>* option_list);
    };
}

#endif // WSCS_CONSOLE_MENU_H
