#ifndef WSCS_CONSOLE_IN_WRITER_H
#define WSCS_CONSOLE_IN_WRITER_H

#include <sstream>

namespace Console {
    class InWriter {
    public:
        // wstrzykiwanie stringow do stdin, do unit testow
        InWriter(InWriter &other) = delete;
        ~InWriter();

        void operator=(const InWriter &) = delete;

        static InWriter* get_instance();
        void clear_stream();
        void switch_stream();
        void set_next(const std::string& next);
        std::string get_next();
    private:
        static InWriter* m_s_singleton;
        std::stringstream m_stream;
        std::streambuf* m_prev;

        InWriter();
    };
}

#endif // WSCS_CONSOLE_IN_WRITER_H
