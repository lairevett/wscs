#ifndef WSCS_CONSOLE_IN_H
#define WSCS_CONSOLE_IN_H

#include <string>
#include <vector>
#include "../validation/base_validator.h"

namespace Console {
    template<typename type>
    class In {
    public:
        In();
        explicit In(const std::string& msg);
        In(const std::string& msg, const std::vector<Validation::BaseValidator<type>*>& validators);

        static type read_raw();
        void validate(bool free = false);
        type read(bool validate = false, bool free_validators = false);
    private:
        type m_value;
        std::string m_msg;
        // bez wskaznika na BaseValidator kazda dziedziczaca instancja jest konwertowana do BaseValidator (slicing)
        std::vector<Validation::BaseValidator<type>*> m_validators;
    };
}

#endif // WSCS_CONSOLE_IN_H
