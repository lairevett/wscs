#ifndef WSCS_CONSOLE_OUT_H
#define WSCS_CONSOLE_OUT_H

#include <iostream>
#include <iomanip>

namespace Console {
    class Out {
    public:
        static void write();

        template<typename Head, typename ...Args>
        inline static void write(Head&& head, Args&& ...args) {
            std::cout << std::fixed << std::setprecision(m_s_precision) << std::forward<Head>(head);
            write(std::forward<Args>(args)...);
        }

        static int set_precision(int precision);
        static void clear();
    private:
        static int m_s_precision;
    };
}

#endif // WSCS_CONSOLE_OUT_H
