#include <limits>
#include <cmath>
#include <string>
#include "float.h"

namespace Util {
    bool Float::is_equal(double a, double b) {
        return fabs(a - b) <= ((fabs(a) > fabs(b) ? fabs(b) : fabs(a)) * std::numeric_limits<double>::epsilon())
               or std::to_string(a) == std::to_string(b);
    }

    bool Float::is_greater(double a, double b) {
        return (a - b) > ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * std::numeric_limits<double>::epsilon());
    }
}
