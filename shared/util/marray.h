#ifndef WSCS_UTIL_ARRAY_H
#define WSCS_UTIL_ARRAY_H

#include <tuple>

namespace Util {
    template<typename type>
    class Array {
    public:
        Array() = delete;
        Array(const Array&) = delete;
        void operator=(const Array&) = delete;

        static int get_length(type arr[], int size);
        static bool contains(type arr[], int size, type value);
        static std::tuple<type, type>* merge_as_tuples(type first_arr[], type second_arr[], int size);
        static type get_max(std::tuple<type, type> arr[], int size);
    };
}

#endif // WSCS_UTIL_ARRAY_H
