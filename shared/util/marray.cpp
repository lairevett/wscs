#include <cstdlib>
#include "marray.h"

namespace Util {
    template bool Array<int>::contains(int arr[], int size, int value);
    template bool Array<std::tuple<int, int>>::contains(
        std::tuple<int, int> arr[],
        int size,
        std::tuple<int, int> value
    );
    template int Array<int>::get_max(std::tuple<int, int> arr[], int size);

    template<typename type>
    int Array<type>::get_length(type arr[], int size) {
        return size / sizeof(arr[0]);
    }

    template<typename type>
    bool Array<type>::contains(type arr[], int size, type value) {
        int length = get_length(arr, size);
        for (int i = 0; i < length; i++) {
            if (arr[i] == value) return true;
        }
        return false;
    }

    template<typename type>
    std::tuple<type, type>* Array<type>::merge_as_tuples(type first_arr[], type second_arr[], int size) {
        auto end_arr = static_cast<std::tuple<type, type>*>(malloc(sizeof(type) * size));
        for (int i = 0; i < size; i++) {
            end_arr[i] = std::make_tuple(first_arr[i], second_arr[i]);
        }
        return &end_arr[0];
    }

    template<typename type>
    type Array<type>::get_max(std::tuple<type, type> arr[], int size) {
        type max = 0;
        for (int i = 0; i < size; i++) {
            type first = std::get<0>(arr[i]);
            type second = std::get<1>(arr[i]);
            if (first > max) max = first;
            if (second > max) max = second;
        }
        return max;
    }
}
