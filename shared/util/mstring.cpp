#include "mstring.h"

namespace Util {
    std::regex String::m_s_newline_re("\n");
    std::regex String::m_s_number_re("\\d+");
    template std::string String::join_vector(std::vector<long long int> &vec, char separator);
    template std::string String::join_vector(std::vector<char> &vec, char separator);

    void String::clear_stream(std::stringstream& stream) {
        stream.clear();
        stream.str(std::string());
    }

    std::string String::escape_newlines(std::string& str) {
        return std::regex_replace(str, m_s_newline_re, "\\n");
    }

    std::string String::escape_newlines(const std::string& str) {
        return std::regex_replace(str, m_s_newline_re, "\\n");
    }

    std::string String::strip_numbers(const std::string& str) {
        return std::regex_replace(str, m_s_number_re, "");
    }

    bool String::ends_with(const std::string &str, const std::string &match) {
        return str.size() >= match.size()
               && str.compare(str.size() - match.size(), match.size(), match) == 0;
    }

    std::string String::join_integers(int limit, int offset, char separator) {
        std::string str;
        for (int i = offset; i < limit + offset; i++) {
            if (i == limit - 1) {
                str += std::to_string(i);
                break;
            }
            str += std::to_string(i) + std::string(&separator, 1) + " ";
        }
        return str;
    }

    template<typename type>
    std::string String::join_vector(std::vector<type>& vec, char separator) {
        std::string str;
        for (auto it = vec.begin(); it != vec.end(); it++) {
            if (it != vec.begin()) str += std::string(&separator, 1) + " ";
            str += std::to_string(*it);
        }
        return str;
    }
}
