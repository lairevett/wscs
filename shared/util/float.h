#ifndef WSCS_UTIL_FLOAT_H
#define WSCS_UTIL_FLOAT_H

namespace Util {
    class Float {
    public:
        Float() = delete;
        Float(const Float&) = delete;
        void operator=(const Float&) = delete;

        static bool is_equal(double a, double b);
        static bool is_greater(double a, double b);
    };
}

#endif // WSCS_UTIL_FLOAT_H
