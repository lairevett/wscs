#ifndef WSCS_UTIL_STRING_H
#define WSCS_UTIL_STRING_H

#include <sstream>
#include <regex>

namespace Util {
    class String {
    public:
        String() = delete;
        String(const String&) = delete;
        void operator=(const String&) = delete;

        static void clear_stream(std::stringstream& stream);
        static std::string escape_newlines(std::string& str);
        static std::string escape_newlines(const std::string& str);
        static std::string strip_numbers(const std::string& str);
        static bool ends_with(const std::string &str, const std::string &match);
        static std::string join_integers(int limit, int offset = 0, char separator = ',');
        template<typename type>
        static std::string join_vector(std::vector<type> &vec, char separator);
    private:
        static std::regex m_s_newline_re;
        static std::regex m_s_number_re;
    };
}

#endif // WSCS_UTIL_STRING_H
