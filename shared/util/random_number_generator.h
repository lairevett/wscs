#ifndef WSCS_UTIL_RANDOM_NUMBER_GENERATOR_H
#define WSCS_UTIL_RANDOM_NUMBER_GENERATOR_H

#include <random>

namespace Util {
    template<typename type>
    class RandomNumberGenerator {
    public:
        RandomNumberGenerator() = delete;
        RandomNumberGenerator(const RandomNumberGenerator<type>&) = delete;
        void operator=(const RandomNumberGenerator<type>&) = delete;

        template<typename T = type, std::enable_if_t<std::is_integral<T>::value, int> = 0>
        static type inline next_int(type min, type max, bool inclusive = true) {
            type effective_max = m_s_get_effective_max(max, inclusive);
            std::uniform_int_distribution<type> dist(min, effective_max);
            return dist(m_s_mersenne_twister_engine);
        }

        template<typename T = type, std::enable_if_t<std::is_floating_point<T>::value, int> = 0>
        static type inline next_real(type min, type max, bool inclusive = true) {
            type effective_max = m_s_get_effective_max(max, inclusive);
            std::uniform_real_distribution<type> dist(min, effective_max);
            return dist(m_s_mersenne_twister_engine);
        };
    private:
        inline static std::random_device m_s_random_device = std::random_device();
        inline static std::mt19937 m_s_mersenne_twister_engine = std::mt19937(m_s_random_device());

        static type inline m_s_get_effective_max(type max, bool inclusive) {
            return inclusive ? std::nextafter(max, std::numeric_limits<type>::max()) : max;
        };
    };
}

#endif // WSCS_UTIL_RANDOM_NUMBER_GENERATOR_H
