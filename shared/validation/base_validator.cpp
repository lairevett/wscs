#include "base_validator.h"
#include "../error/not_implemented.h"

namespace Validation {
    template class BaseValidator<unsigned short>;
    template class BaseValidator<double>;
    template class BaseValidator<long long int>;
    template class BaseValidator<int>;
    template class BaseValidator<char>;

    template<typename type>
    void BaseValidator<type>::validate(type value) {
        throw Error::NotImplemented("Brak override'a metody validate() w klasie");
    }
}
