#ifndef WSCS_VALIDATION_NUMBER_GREATER_VALIDATOR_H
#define WSCS_VALIDATION_NUMBER_GREATER_VALIDATOR_H

#include "base_validator.h"

namespace Validation {
    template<typename type>
    class NumberGreaterValidator : public BaseValidator<type> {
    public:
        explicit NumberGreaterValidator(type than, bool inclusive = false);
        void validate(type value) override;
    private:
        type m_than;
        bool m_inclusive;
    };
}

#endif // WSCS_VALIDATION_NUMBER_GREATER_VALIDATOR_H
