#include "error.h"

Validation::Error::Error(const std::string& msg) :
    std::runtime_error(msg) {
}
