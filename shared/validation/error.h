#ifndef WSCS_VALIDATION_ERROR_H
#define WSCS_VALIDATION_ERROR_H

#include <stdexcept>

namespace Validation {
    class Error : public std::runtime_error {
    public:
        explicit Error(const std::string& msg);
    };
}

#endif // WSCS_VALIDATION_ERROR_H
