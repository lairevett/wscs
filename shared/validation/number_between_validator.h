#ifndef WSCS_VALIDATION_NUMBER_BETWEEN_VALIDATOR_H
#define WSCS_VALIDATION_NUMBER_BETWEEN_VALIDATOR_H

#include "base_validator.h"

namespace Validation {
    template<typename type>
    class NumberBetweenValidator : public BaseValidator<type> {
    public:
        NumberBetweenValidator(type min, type max, bool list_choices = false);
        void validate(type value) override;
    private:
        type m_min;
        type m_max;
        bool m_list_choices;
    };
}

#endif // WSCS_VALIDATION_NUMBER_BETWEEN_VALIDATOR_H
