#ifndef WSCS_VALIDATION_DECIMAL_PLACES_VALIDATOR_H
#define WSCS_VALIDATION_DECIMAL_PLACES_VALIDATOR_H

#include "base_validator.h"

namespace Validation {
    class DecimalPlacesValidator : public BaseValidator<double> {
    public:
        explicit DecimalPlacesValidator(int decimal_places);
        void validate(double value) override;
    private:
        int m_decimal_places;
    };
}

#endif // WSCS_VALIDATION_DECIMAL_PLACES_VALIDATOR_H
