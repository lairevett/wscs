#ifndef WSCS_VALIDATION_CHOICE_VALIDATOR
#define WSCS_VALIDATION_CHOICE_VALIDATOR

#include <vector>
#include "base_validator.h"

namespace Validation {
    template<typename type>
    class ChoiceValidator : public BaseValidator<type> {
    public:
        ChoiceValidator(std::vector<type> choices);
        void validate(type value) override;
    private:
        std::vector<type> m_choices;
    };
}

#endif // WSCS_VALIDATION_CHOICE_VALIDATOR
