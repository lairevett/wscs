#include "choice_validator.h"
#include "error.h"
#include "../util/mstring.h"

namespace Validation {
    template class ChoiceValidator<char>;

    template<typename type>
    ChoiceValidator<type>::ChoiceValidator(std::vector<type> choices) :
            m_choices(choices) {
    }

    template<typename type>
    void ChoiceValidator<type>::validate(type value) {
        if (std::count(m_choices.begin(), m_choices.end(), value) > 0) return;
        throw Error("Nieprawidlowa wartosc, dopuszczalne to " + Util::String::join_vector(m_choices, ','));
    }
}
