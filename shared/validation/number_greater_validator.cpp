#include <sstream>
#include "number_greater_validator.h"
#include "error.h"

namespace Validation {
    template class NumberGreaterValidator<double>;
    template class NumberGreaterValidator<long long int>;
    template class NumberGreaterValidator<int>;

    template<typename type>
    NumberGreaterValidator<type>::NumberGreaterValidator(type than, bool inclusive) :
        m_than(than),
        m_inclusive(inclusive) {
    }

    template<typename type>
    void NumberGreaterValidator<type>::validate(type value) {
        if (m_inclusive && value >= m_than || value > m_than) return;
        std::stringstream ss;
        ss << "Wartosc musi byc wieksza " << (m_inclusive ? "lub rowna " : "niz ") << m_than;
        throw Error(ss.str());
    }
}
