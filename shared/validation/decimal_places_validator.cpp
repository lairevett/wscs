#include <cassert>
#include <string>
#include "../util/float.h"
#include "decimal_places_validator.h"
#include "error.h"

namespace Validation {
    DecimalPlacesValidator::DecimalPlacesValidator(int decimal_places) :
        m_decimal_places(decimal_places) {
        assert(m_decimal_places < 7);
    }

    void DecimalPlacesValidator::validate(double value) {
        auto str = std::to_string(value);
        unsigned long delimiter = str.find('.');
        if (delimiter == std::string::npos) {
            return;
        }
        auto stripped_str = str.erase(str.find_last_not_of('0') + 1, std::string::npos);
        if (Util::Float::is_greater(std::stod(str), value)  // wiecej niz 6 po kropce, to_string automatycznie zaokraglil
            || stripped_str.substr(delimiter + 1, std::string::npos).length() > m_decimal_places) {
            throw Error("Maksymalna ilosc cyfr po kropce dziesietnej to " + std::to_string(m_decimal_places));
        }
    }
}
