#include <string>
#include "number_between_validator.h"
#include "error.h"
#include "../util/mstring.h"

namespace Validation {
    template class NumberBetweenValidator<unsigned short>;
    template class NumberBetweenValidator<double>;
    template class NumberBetweenValidator<int>;

    template<typename type>
    NumberBetweenValidator<type>::NumberBetweenValidator(type min, type max, bool list_choices) :
        m_min(min),
        m_max(max),
        m_list_choices(list_choices) {
    }

    template<typename type>
    void NumberBetweenValidator<type>::validate(type value) {
        if (value >= m_min && value < m_max) return;
        std::string err_msg = "Nieprawidlowa wartosc, dopuszczalne to ";
        err_msg += m_list_choices
                    ? Util::String::join_integers(static_cast<int>(m_max))
                    : ("miedzy " + std::to_string(m_min) + ", a " + std::to_string(m_max));
        throw Error(err_msg);
    }
}
