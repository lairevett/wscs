#ifndef WSCS_VALIDATION_BASE_VALIDATOR_H
#define WSCS_VALIDATION_BASE_VALIDATOR_H

namespace Validation {
    template<typename type>
    class BaseValidator {
    public:
        virtual void validate(type value);
    };
}

#endif // WSCS_VALIDATION_BASE_VALIDATOR_H
