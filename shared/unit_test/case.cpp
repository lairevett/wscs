#include <cassert>
#include "../shared/console/out.h"
#include "../shared/console/in_writer.h"
#include "../shared/console/out_reader.h"
#include "../shared/validation/error.h"
#include "../shared/util/float.h"
#include "../shared/util/mstring.h"
#include "case.h"
#include "result.h"

namespace UnitTest {
    template void Case::assert_throws<Validation::Error>(void (*callback)(), const std::string& expected_msg);
    template void Case::assert_throws<Validation::Error>(
        void (*callback)(),
        const std::string& stdin_,
        const std::string& expected_msg
    );
    template void Case::assert_retval_equals<int>(int (*callback)(), const int& expected_retval);

    std::stringstream* Case::m_s_stream = new std::stringstream;
    int Case::m_s_number = 0;
    int Case::m_s_passed_count = 0;
    int Case::m_s_all_count = 0;

    int Case::get_next_number() {
        return ++m_s_number;
    }

    std::string Case::get_unit_summary(bool condition) {
        m_s_all_count++;
        ResultE result = condition ? PASS : FAIL;
        m_s_passed_count += result;
        Util::String::clear_stream(*m_s_stream);
        *m_s_stream << "#" << get_next_number() << ": " << Result::to_string(result) << std::flush;
        return m_s_stream->str();
    }

    void Case::assert_stdin_equals(void (*callback)(), const std::string& stdin_, const std::string& expected_stdin) {
        auto writer = Console::InWriter::get_instance();
        writer->set_next(stdin_);
        auto reader = Console::OutReader::get_instance();
        callback();
        std::string actual_stdin = writer->get_next();
        reader->switch_buffers();
        Console::Out::write(
            get_unit_summary(actual_stdin == expected_stdin),
            " Oczekuj, aby stdin `", Util::String::escape_newlines(actual_stdin),
            "` rownal sie `", Util::String::escape_newlines(expected_stdin), "`\n"
        );
        reader->switch_buffers();
        reader->clear_stream();
    }

    void Case::assert_stdout_equals(void (*callback)(), const std::string& stdin_, const std::string& expected_stdout) {
        if (stdin_.length() > 0) Console::InWriter::get_instance()->set_next(stdin_);
        auto reader = Console::OutReader::get_instance();
        callback();
        std::string stdout_ = reader->get_last();
        reader->switch_buffers();
        Console::Out::write(
            get_unit_summary(stdout_ == expected_stdout),
            " Oczekuj, aby stdout `", Util::String::escape_newlines(stdout_),
            "`" " rownal sie `", Util::String::escape_newlines(expected_stdout), "`\n"
        );
        reader->switch_buffers();
    }

    template<typename type>
    void Case::assert_retval_equals(type (*callback)(), const type& expected_retval) {
        auto reader = Console::OutReader::get_instance();
        type retval = callback();
        reader->switch_buffers();
        Console::Out::write(
            get_unit_summary(retval == expected_retval),
            " Oczekuj, aby zwrocona wartosc `", retval,
            "`" " rownala sie `", expected_retval, "`\n"
        );
        reader->switch_buffers();
    }

    void Case::assert_float_equals(double value, double expected_value) {
        auto reader = Console::OutReader::get_instance();
        reader->switch_buffers();
        Console::Out::write(
            get_unit_summary(Util::Float::is_equal(value, expected_value)),
            " Oczekuj, aby float `", std::to_string(value),
            "` mniej wiecej rownal sie `", std::to_string(expected_value), "`\n"
        );
        reader->switch_buffers();
        reader->clear_stream();
    }

    void Case::assert_not_throws(void (*callback)()) {
        auto reader = Console::OutReader::get_instance();
        bool exc_thrown = false;
        std::string msg = "<wyjatek nie zostal rzucony, brak wiadomosci>";
        try {
            callback();
        } catch (std::exception& exc) {
            exc_thrown = true;
            msg = exc.what();
        }
        reader->switch_buffers();
        Console::Out::write(
            get_unit_summary(!exc_thrown && msg == "<wyjatek nie zostal rzucony, brak wiadomosci>"),
            " Oczekuj, aby nie rzucono zadnego wyjatku, wiadomosc: `", msg, "`\n"
        );
        reader->switch_buffers();
        reader->clear_stream();
    }

    template<class ExpectedException>
    void Case::assert_throws(void (*callback)(), const std::string& expected_msg) {
        auto reader = Console::OutReader::get_instance();
        bool exc_thrown = false;
        std::string msg = "<wyjatek nie zostal rzucony, brak wiadomosci>";
        try {
            callback();
        } catch (ExpectedException& exc) {
            exc_thrown = true;
            msg = exc.what();
        }
        reader->switch_buffers();
        // typeid().name() zwraca cos typu 15Validation::Error,
        // Util::String::strip_numbers pozbywa sie numeru na poczatku
        Console::Out::write(
            get_unit_summary(exc_thrown && msg == expected_msg),
            " Oczekuj, aby rzucono wyjatek `", Util::String::strip_numbers(typeid(ExpectedException).name()), "`"
            " z wiadomoscia `", msg, "`", " rownajaca sie `", expected_msg, "`\n"
        );
        reader->switch_buffers();
        reader->clear_stream();
    }

    template<class ExpectedException>
    void Case::assert_throws(void (*callback)(), const std::string& stdin_, const std::string& expected_msg) {
        Console::InWriter::get_instance()->set_next(stdin_);
        assert_throws<ExpectedException>(callback, expected_msg);
    }

    std::string Case::get_summary() {
        int failed_count = m_s_all_count - m_s_passed_count;
        assert(failed_count >= 0);
        Util::String::clear_stream(*m_s_stream);
        *m_s_stream << "Podsumowanie: wykonano " << m_s_all_count << " unit testow, w tym "
                    << m_s_passed_count << " poprawnie i " << failed_count << " niepoprawnie";
        return m_s_stream->str();
    }
}
