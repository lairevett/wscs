#ifndef WSCS_UNIT_TEST_RESULT_H
#define WSCS_UNIT_TEST_RESULT_H

#include <string>
#include <map>

namespace UnitTest {
    enum ResultE {FAIL = 0, PASS};

    class Result {
    public:
        Result() = delete;
        Result(const Result&) = delete;
        void operator=(const Result&) = delete;

        static std::string to_string(ResultE value);
    private:
        static std::map<ResultE, std::string> m_str_map;
    };
}

#endif // WSCS_UNIT_TEST_RESULT_H
