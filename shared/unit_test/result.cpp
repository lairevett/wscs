#include "result.h"

namespace UnitTest {
    std::map<ResultE, std::string> Result::m_str_map = {{FAIL, "[FAIL]"}, {PASS, "[PASS]"}};

    std::string Result::to_string(ResultE value) {
        return m_str_map[value];
    }
}
