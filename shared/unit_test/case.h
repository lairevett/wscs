#ifndef WSCS_UNIT_TEST_CASE_H
#define WSCS_UNIT_TEST_CASE_H

#include <string>
#include <sstream>

namespace UnitTest {
    class Case {
    public:
        Case() = delete;
        Case(const Case&) = delete;
        void operator=(const Case&) = delete;

        static void assert_stdin_equals(
            void (*callback)(),
            const std::string &stdin_,
            const std::string &expected_stdin
        );
        static void assert_stdout_equals(
            void (*callback)(),
            const std::string &stdin_,
            const std::string &expected_stdout
        );
        template<typename type>
        static void assert_retval_equals(type (*callback)(), const type &expected_retval);
        static void assert_float_equals(double value, double expected_value);
        static void assert_not_throws(void (*callback)());

        template<class ExpectedException>
        static void assert_throws(void (*callback)(), const std::string &expected_msg);

        template<class ExpectedException>
        static void assert_throws(void (*callback)(), const std::string &stdin_, const std::string &expected_msg);

        static std::string get_summary();
    private:
        static std::stringstream *m_s_stream;
        static int m_s_number;
        static int m_s_passed_count;
        static int m_s_all_count;

        static int get_next_number();
        static std::string get_unit_summary(bool condition);
    };
}

#endif // WSCS_UNIT_TEST_CASE_H
