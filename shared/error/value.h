#ifndef WSCS_ERROR_VALUE_H
#define WSCS_ERROR_VALUE_H

#include <stdexcept>

namespace Error {
    class Value : public std::runtime_error
    {
    public:
        explicit Value(const std::string &arg);
    };
}


#endif // WSCS_ERROR_VALUE_H
