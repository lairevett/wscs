#include "not_implemented.h"

namespace Error {
    NotImplemented::NotImplemented(const std::string &msg) :
        std::logic_error(msg) {
    }
}
