#include "value.h"

namespace Error {
    Value::Value(const std::string &msg) :
            std::runtime_error(msg) {
    }
}
