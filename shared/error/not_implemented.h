#ifndef WSCS_ERROR_NOT_IMPLEMENTED_H
#define WSCS_ERROR_NOT_IMPLEMENTED_H

#include <stdexcept>

namespace Error {
    class NotImplemented : public std::logic_error
    {
    public:
        explicit NotImplemented(const std::string &arg);
    };
}

#endif // WSCS_ERROR_NOT_IMPLEMENTED_H
