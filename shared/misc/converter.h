#ifndef WSCS_MISC_CONVERTER_H
#define WSCS_MISC_CONVERTER_H

#include <cmath>

namespace Converter {
    double celsius_to_fahrenheit(double celsius) {
        return 1.8 * celsius + 32.0;
    }

    double kph_to_mps(double kilometers_per_hour) {
        return kilometers_per_hour * 1000.0 / 3600.0;
    }

    double degrees_to_radians(double degrees) {
        return degrees * M_PI / 180.0;
    }
}

#endif // WSCS_MISC_CONVERTER_H
