// src:
// - https://moodle.wwsi.edu.pl/pluginfile.php/39791/mod_folder/content/0/pp_cw04_1_silnia.cpp
// - https://moodle.wwsi.edu.pl/pluginfile.php/39787/mod_folder/content/0/pp05/pp05_03_ilosc_cyfr_liczby_calkowitej.cpp
// - https://moodle.wwsi.edu.pl/pluginfile.php/39787/mod_folder/content/0/pp05/pp05_04_czy_pierwsza.cpp
// - https://moodle.wwsi.edu.pl/pluginfile.php/39787/mod_folder/content/0/pp05/pp05_05_calka.cpp
#include <iostream>
#include <cmath>
#include <limits>
#include <map>
#include "../shared/console/in.h"
#include "../shared/console/out.h"
#include "../shared/console/out_reader.h"
#include "../shared/console/menu.h"
#include "../shared/validation/error.h"
#include "../shared/validation/number_greater_validator.h"
#include "../shared/unit_test/case.h"
#include "../shared/misc/constants.h"
#include "../shared/util/mstring.h"

long long oblicz_silnie(int n) {
    if (n < 0) return -1;
    if (n > 20) return -2;
    long long s = 1;
    for (int i = 2; i <= n; i++) {
        s = s * i;
    }
    return s;
}

void run_ex_1() {
    long long s;
    int n = Console::In<int>("Podaj n: ").read();
    s = oblicz_silnie(n);
    if (s == -1) Console::Out::write("n nie moze byc ujemne\n");
    else if (s == -2) Console::Out::write("n nie moze byc wieksze niz 20\n");
    else Console::Out::write(n, "! = ", s, "\n");
}

int zlicz_cyfry(long long a){
    int licznik_cyfr;
    if (a == 0) {
        licznik_cyfr = 1;
    } else {
        if (a < 0) a = -a;
        licznik_cyfr = 0;
        while (a > 0) {
            a = a / 10;
            licznik_cyfr = licznik_cyfr + 1;
        }
    }
    return licznik_cyfr;
}

double oblicz_pierwiastek(double x, double epsilon) {
//    int loop_count = 0;
    double w = 1.0;
    while (std::abs(std::pow(w, 2) - x) >= epsilon) {
//        loop_count++;
        w = ((x / w) + w) / 2;
    }
//    Console::Out::write("Liczba obrotow: ", loop_count, "\n");
    return w;
}

void run_ex_3() {
    int prev = Console::Out::set_precision(8);
    std::vector<double> epsilon_checks = {
            0.1,   // Liczba obrotow: 5
            0.01,  // Liczba obrotow: 5
            0.001, // Liczba obrotow: 5
            0.0001 // Liczba obrotow: 6
    };
    for (auto& epsilon : epsilon_checks) Console::Out::write("x = ", oblicz_pierwiastek(25, epsilon), "\n");
    Console::Out::set_precision(prev);
}

long double xtpp(long double x, long long int p) {
    long double retval = 1.0;
    if (p > 0) {
        // 2 ^ 10 = 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2
        //        = (2 * 2) * (2 * 2) * (2 * 2) * (2 * 2) * (2 * 2)
        //        = ((2 * 2) ^ 5)
        //        = 4 ^ 5 --> retval
        // x jest mnozone przez x, p jest dzielone na 2 co kazda iteracje
        // potega jest teraz nieparzysta, ale to mozna zapisac po prostu jako (4 ^ 4) * 4, czyli retval * x
        // (4 ^ 4) * 4 = (16 ^ 2) * 4
        // (16 ^ 2) * 4 = (256 ^ 1) * 4
        // (256 ^ 1) * 4 = 256 * 4 = 1024
        // zlozonosc to tylko O(log p) dzieki polowieniu p
        for (; p; x *= x, p >>= 1) {
            if (p & 1) retval *= x;
        }
    } else if (p < 0) {
        return 1.0 / pow(x, -p);
    }
    return retval;
}

void run_ex_4() {
    int prev = Console::Out::set_precision(3);
    std::vector<std::pair<double, int>> args_checks = {
        {123, 0},
        {-123, 0},
        {2, 10},
        {2, -10},
        {-2, 10},
        {-2, -10},
        {2.5, 10},
        {2.5, -10},
        {-2.5, 10},
        {-2.5, -10},
        {2, 10000},
    };
    for (auto& args : args_checks) {
        Console::Out::write("xtpp(", args.first, ", ", args.second, ") = ", xtpp(args.first, args.second), "\n");
    }
    Console::Out::set_precision(prev);
}

bool czy_pierwsza(long long a) {
    //zwraca true, jezeli a jest liczba pierwsza
    //zwraca false, jezeli a nie jest liczba pierwsza
    bool pierwsza = 1;
    for (int i = 2; i*i <= a; i++) {
        if (a%i == 0) {
            pierwsza = 0;
            break;
        }
    }
    return pierwsza;
}

void run_ex_5() {
    long long int lower_bound = Console::In<long long int>(
        "Podaj poczatek zakresu: ", {
            new Validation::NumberGreaterValidator<long long int>(1)
        }
    ).read(true, true);
    long long int upper_bound = Console::In<long long int>(
        "Podaj koniec zakresu: ", {
            new Validation::NumberGreaterValidator<long long int>(lower_bound - 1)
        }
    ).read(true, true);
    std::vector<long long int> primes;
    for (long long int i = lower_bound; i <= upper_bound; i++) {
        if (!czy_pierwsza(i)) continue;
        primes.push_back(i);
    }
    Console::Out::write(
        "Liczby pierwsze w zakresie ", lower_bound, " do ", upper_bound, ": ",
        primes.empty() ? "brak" : Util::String::join_vector<long long int>(primes, ','),
        "\n"
    );
}

double oblicz_calke(double (*f)(double), double a, double b) {
    double dx = 0.001;
    int n = (b - a) / dx;  // a, b - granice calkowania
    double calka = 0;
    for (int i = 0; i < n; i++) {
        double kawalek = (f(a + i * dx) + f(a + (i + 1)*dx))*dx / 2;
        calka += kawalek;
    }
    return calka;
}

void run_ex_6() {
    int prev = Console::Out::set_precision(2);
    double pp_a = oblicz_calke([](double x) {
        return std::pow(x, 3) - (3 * std::pow(x, 2)) + x + 5;
    }, -1, 4);
    double pp_b = oblicz_calke([](double x) { return std::pow(x, 3); }, -10, 15);
    Console::Out::write("f(x) = x^3 - 3x^2 + x + 5, x = -1, x = 4 -- ", pp_a, "\n");
    Console::Out::write("f(x) = x^3, x = -10, x = 15 -- ", pp_b, "\n");
    Console::Out::set_precision(prev);
}

void display_main_menu() {
    Console::Menu::capture(new std::vector<Console::MenuOption>{
        std::make_pair("Wyjscie", Console::Menu::release),
        std::make_pair("Zadanie 1", run_ex_1),
        std::make_pair("Zadanie 2", []() {
            Console::Out::write(
                "w zadaniu nie trzeba wysylac niczego do stdouta, funkcja jest otestowana w run_ex_2_unit_tests()\n"
            );
        }),
        std::make_pair("Zadanie 3", run_ex_3),
        std::make_pair("Zadanie 4", run_ex_4),
        std::make_pair("Zadanie 5", run_ex_5),
        std::make_pair("Zadanie 6", run_ex_6)
    });
}

void run_ex_1_unit_tests() {
    UnitTest::Case::assert_stdout_equals(run_ex_1, "-1\n", "n nie moze byc ujemne\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "21\n", "n nie moze byc wieksze niz 20\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "0\n", "0! = 1\n");
    UnitTest::Case::assert_stdout_equals(run_ex_1, "20\n", "20! = 2432902008176640000\n");
}

void run_ex_2_unit_tests() {
    UnitTest::Case::assert_retval_equals<int>([]() { return zlicz_cyfry(0); }, 1);
    UnitTest::Case::assert_retval_equals<int>([]() { return zlicz_cyfry(-7624); }, 4);
    UnitTest::Case::assert_retval_equals<int>([]() { return zlicz_cyfry(7624); }, 4);
}

void run_ex_3_unit_tests() {
    UnitTest::Case::assert_stdout_equals(
        run_ex_3,
        "",
        "x = 5.00002318\nx = 5.00002318\nx = 5.00002318\nx = 5.00000000\n"
    );
}

void run_ex_4_unit_tests() {
    UnitTest::Case::assert_stdout_equals(
        run_ex_4,
        "",
        "xtpp(123.000, 0) = 1.000\n"
        "xtpp(-123.000, 0) = 1.000\n"
        "xtpp(2.000, 10) = 1024.000\n"
        "xtpp(2.000, -10) = 0.001\n"
        "xtpp(-2.000, 10) = 1024.000\n"
        "xtpp(-2.000, -10) = 0.001\n"
        "xtpp(2.500, 10) = 9536.743\n"
        "xtpp(2.500, -10) = 0.000\n"
        "xtpp(-2.500, 10) = 9536.743\n"
        "xtpp(-2.500, -10) = 0.000\n"
        "xtpp(2.000, 10000) = 19950631168807583848837421626835850838234968318861924548520089498529438830221946631919961684036194597899331129423209124271556491349413781117593785932096323957855730046793794526765246551266059895520550086918193311542508608460618104685509074866089624888090489894838009253941633257850621568309473902556912388065225096643874441046759871626985453222868538161694315775629640762836880760732228535091641476183956381458969463899410840960536267821064621427333394036525565649530603142680234969400335934316651459297773279665775606172582031407994198179607378245683762280037302885487251900834464581454650557929601414833921615734588139257095379769119277800826957735674444123062018757836325502728323789270710373802866393031428133241401624195671690574061419654342324638801248856147305207431992259611796250130992860241708340807605932320161268492288496255841312844061536738951487114256315111089745514203313820202931640957596464756010405845841566072044962867016515061920631004186422275908670900574606417856951911456055068251250406007519842261898059237118054444788072906395242548339221982707404473162376760846613033778706039803413197133493654622700563169937455508241780972810983291314403571877524768509857276937926433221599399876886660808368837838027643282775172273657572744784112294389733810861607423253291974813120197604178281965697475898164531258434135959862784130128185406283476649088690521047580882615823961985770122407044330583075869039319604603404973156583208672105913300903752823415539745394397715257455290510212310947321610753474825740775273986348298498340756937955646638621874569499279016572103701364433135817214311791398222983845847334440270964182851005072927748364550578634501100852987812389473928699540834346158807043959118985815145779177143619698728131459483783202081474982171858011389071228250905826817436220577475921417653715687725614904582904992461028630081535583308130101987675856234343538955409175623400844887526162643568648833519463720377293240094456246923254350400678027273837755376406726898636241037491410966718557050759098100246789880178271925953381282421954028302759408448955014676668389697996886241636313376393903373455801407636741877711055384225739499110186468219696581651485130494222369947714763069155468217682876200362777257723781365331611196811280792669481887201298643660768551639860534602297871557517947385246369446923087894265948217008051120322365496288169035739121368338393591756418733850510970271613915439590991598154654417336311656936031122249937969999226781732358023111862644575299135758175008199839236284615249881088960232244362173771618086357015468484058622329792853875623486556440536962622018963571028812361567512543338303270029097668650568557157505516727518899194129711337690149916181315171544007728650573189557450920330185304847113818315407324053319038462084036421763703911550639789000742853672196280903477974533320468368795868580237952218629120080742819551317948157624448298518461509704888027274721574688131594750409732115080498190455803416826949787141316063210686391511681774304792596709376.000\n"
    );
}

void run_ex_5_unit_tests() {
    UnitTest::Case::assert_throws<Validation::Error>(run_ex_5, "1\n2\n", "Wartosc musi byc wieksza niz 1");
    UnitTest::Case::assert_throws<Validation::Error>(run_ex_5, "2\n1\n", "Wartosc musi byc wieksza niz 1");
    UnitTest::Case::assert_stdout_equals(run_ex_5, "2\n2\n", "Liczby pierwsze w zakresie 2 do 2: 2\n");
    UnitTest::Case::assert_stdout_equals(run_ex_5, "4\n4\n", "Liczby pierwsze w zakresie 4 do 4: brak\n");
    UnitTest::Case::assert_stdout_equals(run_ex_5, "21\n30\n", "Liczby pierwsze w zakresie 21 do 30: 23, 29\n");
}

void run_ex_6_unit_tests() {
    UnitTest::Case::assert_stdout_equals(
        run_ex_6,
        "",
        "f(x) = x^3 - 3x^2 + x + 5, x = -1, x = 4 -- 31.25\n"
        // https://www.wolframalpha.com/input/?i=integrate+x%5E3+dx+from+-10+to+15 wskazuje 10156.25
        // blad w pp cw04.docx? tam jest 15156.25
        "f(x) = x^3, x = -10, x = 15 -- 10156.25\n"
    );
}

int main() {
    Console::Out::set_precision(std::numeric_limits<float>::digits10);
#ifdef NDEBUG
    display_main_menu();
#else
    run_ex_1_unit_tests();
    run_ex_2_unit_tests();
    run_ex_3_unit_tests();
    run_ex_4_unit_tests();
    run_ex_5_unit_tests();
    run_ex_6_unit_tests();
    auto reader = Console::OutReader::get_instance();
    reader->switch_buffers();
    std::cout << UnitTest::Case::get_summary() << std::endl;
    reader->switch_buffers();
#endif
    return EXIT_SUCCESS;
}