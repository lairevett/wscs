/*EFigielska pp2018*/
#pragma once
#include <cassert>
#include <cmath>
#include <algorithm>
int * utworz_int(int n);//tworzy tablice z liczbami calkowitymi
double * utworz_double(int n);//tworzy tablice z liczbami rzeczywistymi
void wygeneruj_wartosci(int * a, int n, int d=0, int g=10); //do generowania losowo
void wygeneruj_wartosci(double * a, int n, double d = 0, double g = 10);
void wpisz_wartosci(int * a, int n); //do wpisywania z klawiatury
void wpisz_wartosci(double * a, int n);
void drukuj_tablice(int * a, int n);
void drukuj_tablice(double * a, int n);
bool jest_scisle_rosnacy(int * a, int n);
bool jest_scisle_rosnacy(double * a, int n);
bool jest_scisle_malejacy(int * a, int n);
bool jest_scisle_malejacy(double * a, int n);
//int znajdz_wartosc(int * a, int n, int x);
int znajdz_wartosc(double * a, int n, double x, double eps);
//int wyznacz_min(int *a, int n);
//double wyznacz_min(double *a, int n);
int get_max(int* a, int n);
double get_max(double* a, int n);
//bool jest_arytmetyczny(int* a, int n);
//bool jest_arytmetyczny(double* a, int n, double eps);

template<typename type>
inline type calc_polynomial(type a[], int n) {
    assert(n > 0);
    double sum = a[0];
    for (int i = 1; i < n; i++) {
        sum += std::pow(a[i], i + 1);
    }
    return sum;
}

enum class SortDirection { DESCENDING = 0, ASCENDING };

template<typename type>
inline type* quick_sort(type arr[], int left, int right, SortDirection dir) {
    int i = left;
    int j = right;
    type pivot = arr[(left + right) / 2];
    for (;;) {
        while (dir == SortDirection::ASCENDING == arr[i] < pivot && arr[i] != pivot) i++;
        while (dir == SortDirection::ASCENDING == arr[j] > pivot && arr[j] != pivot) j--;
        if (i <= j) {
            std::swap(arr[i], arr[j]);
            i++;
            j--;
            continue;
        }
        break;
    }
    if (left < j) quick_sort(arr, left, j, dir);
    if (right > i) quick_sort(arr, i, right, dir);
}
