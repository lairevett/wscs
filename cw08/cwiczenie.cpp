/*EFigielska pp2018*/
#include "dzialania.h"
#include <iostream>
#include <cstring>
using namespace std;
int main() {
	int n;
	cout << "podaj rozmiar tablic: ";
	cin >> n;
	int * ti = utworz_int(n); //tablica z liczbami calkowitymi
	double * td = utworz_double(n); //tablica z liczbami rzeczywistymi
	
	char wybor = 'l';
	cout << "tablice losowe (l) czy z klawiatury (k)?: ";
	cin >> wybor;
	if (wybor == 'l') {
		wygeneruj_wartosci(ti, n);
		wygeneruj_wartosci(td, n);
	}
	else {
		wpisz_wartosci(ti, n);
		wpisz_wartosci(td, n);
	}

	drukuj_tablice(ti, n);
	if (jest_scisle_rosnacy(ti, n)) {
		cout << "\nciag liczb w tablicy jest scisle rosnacy";
	}
	else
		cout << "\nciag liczb w tablicy nie jest scisle rosnacy";

    if (jest_scisle_malejacy(ti, n)) {
        cout << "\nciag liczb w tablicy jest scisle malejacy";
    }
    else
        cout << "\nciag liczb w tablicy nie jest scisle malejacy";

    cout << "\nmax: " << get_max(ti, n);
    cout << "\nwartosc wielomianu (x^1 -> x^n): " << calc_polynomial(ti, n);
    int sizei = n * sizeof(int);
    auto sti = static_cast<int*>(malloc(sizei));
    memcpy(sti, ti, sizei);
    quick_sort<int>(sti, 0, n - 1, SortDirection::ASCENDING);
    drukuj_tablice(sti, n);
    quick_sort<int>(sti, 0, n - 1, SortDirection::DESCENDING);
    drukuj_tablice(sti, n);
    free(sti);

	drukuj_tablice(td, n);
	if (jest_scisle_rosnacy(td, n)) {
		cout << "\nciag liczb w tablicy jest scisle rosnacy";
	}
	else
		cout << "\nciag liczb w tablicy nie jest scisle rosnacy";

    if (jest_scisle_malejacy(td, n)) {
        cout << "\nciag liczb w tablicy jest scisle malejacy";
    }
    else
        cout << "\nciag liczb w tablicy nie jest scisle malejacy";

    cout << "\nmax: " << get_max(td, n);
    cout << "\nwartosc wielomianu (x^1 -> x^n): " << calc_polynomial(td, n);
    int sized = n * sizeof(double);
    auto std = static_cast<double*>(malloc(sized));
    memcpy(std, td, sized);
    quick_sort<double>(std, 0, n - 1, SortDirection::ASCENDING);
    drukuj_tablice(std, n);
    quick_sort<double>(std, 0, n - 1, SortDirection::DESCENDING);
    drukuj_tablice(std, n);
    free(std);

	double szukana;
	cout << "\npodaj wartosc szukana: ";
	cin >> szukana;
	int indeks = znajdz_wartosc(td, n, szukana, 0.01);
	if (indeks < 0) {
		cout << "brak wartosci " << szukana << " w tablicy";
	}
	else
		cout << "wartosc " << szukana << " jest na pozycji " << indeks;

}